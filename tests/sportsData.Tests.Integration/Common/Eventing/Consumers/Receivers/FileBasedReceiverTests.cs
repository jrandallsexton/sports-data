﻿using FluentAssertions;

using sportsData.Common.Eventing.Consumers.Receivers;
using sportsData.Common.IO;

using System.Threading.Tasks;

using Xunit;

namespace sportsData.Tests.Integration.Common.Eventing.Consumers.Receivers
{
    public class FileBasedReceiverTests
    {
        private const string InputDir = @"C:\Projects\sports-data\topics";

        [Fact]
        public async Task Receiver_Receives()
        {
            // Arrange
            var fileIoProvider = new FileIoProvider();

            var receiver = new FileBasedReceiver(InputDir, fileIoProvider);

            // Act
            var events = await receiver.Receive();

            // Assert
            events.Should().NotBeNull();
            events.Count.Should().Be(1);
        }
    }
}