﻿using Microsoft.EntityFrameworkCore;

using Moq.AutoMock;

using sportsData.Common.Eventing;
using sportsData.Common.Eventing.Providers;
using sportsData.Data;

using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using sportsData.Events;

namespace sportsData.Tests
{
    public abstract class UnitTestBase<T>
    {

        public int ValidUserId = 99;

        public static SportsDataContext GetDataContext()
        {
            return new SportsDataContext(GetDataContextOptions<SportsDataContext>());
        }

        public static EventingDataContext GetEventingDataContext()
        {
            return new EventingDataContext(GetDataContextOptions<EventingDataContext>());
        }

        public static DbContextOptions<TDc> GetDataContextOptions<TDc>() where TDc : DbContext
        {
            // https://stackoverflow.com/questions/52810039/moq-and-setting-up-db-context
            var dbName = Guid.NewGuid().ToString().Substring(0, 5);
            return new DbContextOptionsBuilder<TDc>()
                .UseInMemoryDatabase(dbName)
                .Options;
        }

        public async Task<AutoMocker> GetDefaultMocker()
        {
            var mocker = new AutoMocker();

            return mocker;
        }

        public async Task AssertCreatedEventCount<TE>(IProvideEventingData dataContact, int expectedCount) where TE : EventBase
        {
            var eventCount = await GetCreatedEventCount<TE>(dataContact);
            eventCount.Should().Be(expectedCount);
        }

        private static async Task<int> GetCreatedEventCount<TE>(IProvideEventingData dataContext) where TE : EventBase
        {
            var outboxMessages = await dataContext.OutgoingEvents
                .AsNoTracking()
                .Where(m => m.EventType == typeof(TE).Name)
                .ToListAsync();

            return outboxMessages?.Count ?? 0;
        }

        public async Task AssertReceivedEventCount<TE>(IProvideEventingData dataContact, int expectedCount) where TE : EventBase
        {
            var eventCount = await GetReceivedEventCount<TE>(dataContact);
            eventCount.Should().Be(expectedCount);
        }

        private static async Task<int> GetReceivedEventCount<TE>(IProvideEventingData dataContext) where TE : EventBase
        {
            var outboxMessages = await dataContext.IncomingEvents
                .AsNoTracking()
                .Where(m => m.EventType == typeof(TE).Name)
                .ToListAsync();

            return outboxMessages?.Count ?? 0;
        }
    }

    public class EventingDataContext : DbContext, IProvideEventingData
    {
        public EventingDataContext(DbContextOptions<EventingDataContext> options)
            : base(options) { }

        public DbSet<OutgoingEvent> OutgoingEvents { get; set; }
        public DbSet<IncomingEvent> IncomingEvents { get; set; }

        public async Task<int> SaveChanges(CancellationToken cancellationToken = default)
        {
            return await base.SaveChangesAsync(cancellationToken);
        }
        public new void Dispose()
        {
            base.Dispose();
        }
    }
}