﻿using FluentAssertions;

using sportsData.Common.ExtensionMethods;
using sportsData.Common.Hashing;
using sportsData.Infrastructure.Espn.Dtos.Venue;

using System;
using System.Collections.Generic;

using Xunit;

namespace sportsData.Tests.Common.Hashing
{
    public class Md5HashProviderTests : UnitTestBase<Md5HashProvider>
    {
        [Fact]
        public void JsonIsEqual_HashesAreEqual()
        {
            // Arrange
            var venue0 = new Venue()
            {
                Id = 1,
                Capacity = 99999,
                FullName = "fullName-value",
                Grass = true,
                Indoor = false,
                Ref = new Uri("http://foo.com"),
                ShortName = "shortName-value",
                Address = new sportsData.Infrastructure.Espn.Dtos.Venue.Address()
                {
                    City = "city-value",
                    State = "state-value",
                    ZipCode = 33990
                },
                Images = new List<sportsData.Infrastructure.Espn.Dtos.Venue.Image>()
                {
                    new sportsData.Infrastructure.Espn.Dtos.Venue.Image()
                    {
                        Alt = "alt-value",
                        Height = 480,
                        Href = new Uri("http://foo.com/img.jpg"),
                        Width = 680,
                        Rel = new List<string>() {"inside", "full-view"}
                    }
                }
            };

            var venue1 = new Venue()
            {
                Id = 1,
                Capacity = 99999,
                FullName = "fullName-value",
                Grass = true,
                Indoor = false,
                Ref = new Uri("http://foo.com"),
                ShortName = "shortName-value",
                Address = new sportsData.Infrastructure.Espn.Dtos.Venue.Address()
                {
                    City = "city-value",
                    State = "state-value",
                    ZipCode = 33990
                },
                Images = new List<sportsData.Infrastructure.Espn.Dtos.Venue.Image>()
                {
                    new sportsData.Infrastructure.Espn.Dtos.Venue.Image()
                    {
                        Alt = "alt-value",
                        Height = 480,
                        Href = new Uri("http://foo.com/img.jpg"),
                        Width = 680,
                        Rel = new List<string>() {"inside", "full-view"}
                    }
                }
            };

            var hashProvider = new Md5HashProvider();

            // Act
            var hash0 = hashProvider.GenerateHash(venue0.ToJson());
            var hash1 = hashProvider.GenerateHash(venue1.ToJson());

            // Assert
            hash0.Should().BeEquivalentTo(hash1);
        }
        [Fact]
        public void JsonIsNotEqual_HashesAreNotEqual()
        {
            // Arrange
            var venue0 = new Venue()
            {
                Id = 1,
                Capacity = 99999,
                FullName = "fullName-value",
                Grass = true,
                Indoor = false,
                Ref = new Uri("http://foo.com"),
                ShortName = "shortName-value",
                Address = new sportsData.Infrastructure.Espn.Dtos.Venue.Address()
                {
                    City = "city-value",
                    State = "state-value",
                    ZipCode = 33990
                },
                Images = new List<sportsData.Infrastructure.Espn.Dtos.Venue.Image>()
                {
                    new sportsData.Infrastructure.Espn.Dtos.Venue.Image()
                    {
                        Alt = "alt-value",
                        Height = 480,
                        Href = new Uri("http://foo.com/img.jpg"),
                        Width = 680,
                        Rel = new List<string>() {"inside", "full-view"}
                    }
                }
            };

            var venue1 = new Venue()
            {
                Id = 1,
                Capacity = 100000,
                FullName = "fullName-value",
                Grass = true,
                Indoor = false,
                Ref = new Uri("http://foo.com"),
                ShortName = "shortName-value",
                Address = new sportsData.Infrastructure.Espn.Dtos.Venue.Address()
                {
                    City = "city-value",
                    State = "state-value",
                    ZipCode = 33990
                },
                Images = new List<sportsData.Infrastructure.Espn.Dtos.Venue.Image>()
                {
                    new sportsData.Infrastructure.Espn.Dtos.Venue.Image()
                    {
                        Alt = "alt-value",
                        Height = 480,
                        Href = new Uri("http://foo.com/img.jpg"),
                        Width = 680,
                        Rel = new List<string>() {"inside", "full-view"}
                    }
                }
            };

            var hashProvider = new Md5HashProvider();

            // Act
            var hash0 = hashProvider.GenerateHash(venue0.ToJson());
            var hash1 = hashProvider.GenerateHash(venue1.ToJson());

            // Assert
            hash0.Should().NotBeEquivalentTo(hash1);
        }
    }
}