﻿using Castle.Components.DictionaryAdapter;

using FluentAssertions;

using Microsoft.EntityFrameworkCore;

using Moq;
using Moq.AutoMock;

using sportsData.Common.Eventing;
using sportsData.Common.Eventing.Consumers;
using sportsData.Common.Eventing.Consumers.Receivers;
using sportsData.Common.Eventing.Providers;
using sportsData.Common.ExtensionMethods;
using sportsData.Events;

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Xunit;

namespace sportsData.Tests.Common.Eventing.Consumers
{
    public class EventConsumerTests : UnitTestBase<EventConsumer>
    {
        private int _mockedExternalEventHandlerCalledCount = 0;

        [Fact]
        public async Task ConsumeAsync_NullEvents_Handles()
        {
            var mocker = await GetDefaultMocker();

            // Arrange
            await using var context = GetEventingDataContext();
            mocker.Use(typeof(IProvideEventingData), context);

            var consumer = mocker.CreateInstance<EventConsumer>();

            // Act
            await consumer.ConsumeAsync(CancellationToken.None);

            // Assert
            Assert.True(true);
        }

        [Fact]
        public async Task ConsumeAsync_NoEvents_Handles()
        {
            var mocker = await GetDefaultMocker();

            // Arrange
            mocker.GetMock<IReceiveEvents>()
                .Setup(s => s.Receive())
                .ReturnsAsync(new EditableList<EventingBase>());

            await using var context = GetEventingDataContext();
            mocker.Use(typeof(IProvideEventingData), context);

            var consumer = mocker.CreateInstance<EventConsumer>();

            // Act
            await consumer.ConsumeAsync(CancellationToken.None);

            // Assert
            Assert.True(true);
        }

        [Fact]
        public async Task ConsumeAsync_NoHandler_Handles()
        {
            var mocker = await GetDefaultMocker();

            // Arrange
            var mockedEvent = new MockedExternalEvent(Guid.NewGuid(), Guid.NewGuid())
            {
                Id = Guid.NewGuid(),
                Name = "name-value",
                Color = "color-value"
            };

            var incomingEvent = new IncomingEvent()
            {
                CausationId = mockedEvent.CausationId,
                CorrelationId = mockedEvent.CorrelationId,
                CreatedBy = ValidUserId,
                CreatedUtc = DateTime.UtcNow,
                EventPayload = mockedEvent.ToJson(),
                EventType = nameof(MockedExternalEvent),
                Id = Guid.NewGuid()
            };

            mocker.GetMock<IReceiveEvents>()
                .Setup(s => s.Receive())
                .ReturnsAsync(new EditableList<EventingBase>()
                {
                    incomingEvent
                });

            await using var context = GetEventingDataContext();
            mocker.Use(typeof(IProvideEventingData), context);

            var consumer = mocker.CreateInstance<EventConsumer>();

            // Act
            await consumer.ConsumeAsync(CancellationToken.None);

            // Assert
            await AssertReceivedEventCount<MockedExternalEvent>(context, 1);
        }

        [Fact]
        public async Task ConsumeAsync_Handler_Handles()
        {
            var mocker = await GetDefaultMocker();

            // Arrange
            var eventHandlers = new Dictionary<string, Func<string, Task>>
            {
                {nameof(MockedExternalEvent), MockedExternalEventHandler}
            };

            mocker.GetMock<IProvideEventHandlers>()
                .Setup(s => s.GetEventHandlers())
                .Returns(eventHandlers);

            var mockedEvent = new MockedExternalEvent(Guid.NewGuid(), Guid.NewGuid())
            {
                Id = Guid.NewGuid(),
                Name = "name-value",
                Color = "color-value"
            };

            var incomingEvent = new IncomingEvent()
            {
                CausationId = mockedEvent.CausationId,
                CorrelationId = mockedEvent.CorrelationId,
                CreatedBy = ValidUserId,
                CreatedUtc = DateTime.UtcNow,
                EventPayload = mockedEvent.ToJson(),
                EventType = nameof(MockedExternalEvent),
                Id = Guid.NewGuid()
            };

            mocker.GetMock<IReceiveEvents>()
                .Setup(s => s.Receive())
                .ReturnsAsync(new EditableList<EventingBase>()
                {
                    incomingEvent
                });

            await using var context = GetEventingDataContext();
            mocker.Use(typeof(IProvideEventingData), context);

            var consumer = mocker.CreateInstance<EventConsumer>();

            // Act
            await consumer.ConsumeAsync(CancellationToken.None);

            // Assert
            await AssertReceivedEventCount<MockedExternalEvent>(context, 1);
            _mockedExternalEventHandlerCalledCount.Should().Be(1);
            incomingEvent = await context.IncomingEvents.FirstAsync(e => e.Id == incomingEvent.Id);
            incomingEvent.LockedUtc.Should().BeNull();
            incomingEvent.Processed.Should().BeTrue();
            incomingEvent.ProcessedUtc.Should().NotBeNull();
        }

        private async Task MockedExternalEventHandler(string someValue)
        {
            await Task.Delay(100);
            var mockedEvent = someValue.FromJson<MockedExternalEvent>();
            mockedEvent.Name.Should().Be("name-value");
            mockedEvent.Color.Should().Be("color-value");
            _mockedExternalEventHandlerCalledCount++;
        }

        private new async Task<AutoMocker> GetDefaultMocker()
        {
            var mocker = await base.GetDefaultMocker();

            mocker.GetMock<IProvideEventHandlers>()
                .Setup(s => s.GetEventHandlers())
                .Returns(new Dictionary<string, Func<string, Task>>());

            mocker.GetMock<IReceiveEvents>()
                .Setup(s => s.Receive())
                .ReturnsAsync(() => null);

            return mocker;
        }

        private class MockedExternalEvent : EventBase
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public string Color { get; set; }

            public MockedExternalEvent(Guid correlationId, Guid causationId) :
                base(correlationId, causationId, EventingMethod.Created)
            {
            }
        }
    }
}