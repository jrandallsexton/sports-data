﻿using System;
using System.Threading;
using Moq.AutoMock;

using sportsData.Common.Eventing;
using sportsData.Common.Eventing.Publishers;

using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Moq;
using sportsData.Common.Eventing.Providers;
using sportsData.Common.Eventing.Publishers.Broadcasters;
using Xunit;

namespace sportsData.Tests.Common.Eventing.Publishers
{
    public class EventPublisherTests : UnitTestBase<EventPublisher>
    {
        private int _broadcastCalledCount = 0;

        [Fact]
        public async Task Publish_NothingToPublish_Exits()
        {
            var mocker = await GetDefaultMocker();

            // Arrange
            await using var context = GetEventingDataContext();
            mocker.Use(typeof(IProvideEventingData), context);

            var outgoingEvent = new OutgoingEvent()
            {
                Id = Guid.NewGuid(),
                CausationId = Guid.NewGuid(),
                CorrelationId = Guid.NewGuid(),
                CreatedBy = 99,
                CreatedUtc = DateTime.UtcNow,
                EventPayload = "event-payload",
                EventType = "event-type"
            };
            await context.OutgoingEvents.AddAsync(outgoingEvent);
            await context.SaveChangesAsync();

            var publisher = mocker.CreateInstance<EventPublisher>();

            // Act
            await publisher.PublishAsync(CancellationToken.None);

            // Assert
            _broadcastCalledCount.Should().Be(1);

            outgoingEvent = await context.OutgoingEvents.FirstAsync(e => e.Id == outgoingEvent.Id);
            outgoingEvent.Raised.Should().BeTrue();
            outgoingEvent.RaisedUtc.Should().NotBeNull();
        }

        private new async Task<AutoMocker> GetDefaultMocker()
        {
            var mocker = await base.GetDefaultMocker();

            mocker.GetMock<IBroadcastEvents>()
                .Setup(s => s.Broadcast(It.IsAny<OutgoingEvent>()))
                .Callback(() => _broadcastCalledCount++);

            return mocker;
        }
    }
}