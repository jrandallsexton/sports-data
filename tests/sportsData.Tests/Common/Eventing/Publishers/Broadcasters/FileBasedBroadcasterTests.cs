﻿
using sportsData.Common.Eventing;
using sportsData.Common.Eventing.Publishers.Broadcasters;
using sportsData.Common.ExtensionMethods;
using sportsData.Events.Venue;
using sportsData.Events.Venue.DTOs;

using System;
using System.Threading.Tasks;

using Xunit;

namespace sportsData.Tests.Common.Eventing.Publishers.Broadcasters
{
    public class FileBasedBroadcasterTests
    {
        [Fact]
        public async Task Publisher_Publishes()
        {
            // Arrange
            var outgoingEvent = new VenueCreatedEvent(Guid.NewGuid(), Guid.NewGuid())
            {
                Venue = new Venue()
                {
                    Id = 1.ToString(),
                    FullName = "fullName-value"
                }
            };

            var broadcaster = new FileBasedBroadcaster(null, @"C:\Projects\sports-data\topics");

            // Act
            await broadcaster.Broadcast(new OutgoingEvent()
            {
                CausationId = outgoingEvent.CausationId,
                CorrelationId = outgoingEvent.CorrelationId,
                CreatedUtc = DateTime.UtcNow,
                CreatedBy = 99,
                EventType = nameof(VenueCreatedEvent),
                EventPayload = outgoingEvent.ToJson()
            });

            // Assert
        }
    }
}