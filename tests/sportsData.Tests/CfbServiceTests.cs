﻿
using Moq.AutoMock;

using System.Threading.Tasks;

using Xunit;

using Assert = Xunit.Assert;

namespace sportsData.Tests
{

    public class CfbServiceTests
    {
        [Fact]
        public void AreAnyGamesInProgress_Passes()
        {
            // Arrange
            var mocker = new AutoMocker();

            var service = mocker.CreateInstance<CfbService>();

            // Act
            var inProgress = service.AreAnyGamesInProgress();

            // Assert
            Assert.False(inProgress);
        }

        [Fact]
        public void LeagueHierarchy_Passes()
        {
            // Arrange
            var mocker = new AutoMocker();

            var service = mocker.CreateInstance<CfbService>();

            // Act
            var leagueHierarchy = service.LeagueHierarchy();

            // Assert
            Assert.NotNull(leagueHierarchy);
        }

        [Fact]
        public void CurrentSeason_Passes()
        {
            // Arrange
            var mocker = new AutoMocker();

            var service = mocker.CreateInstance<CfbService>();

            // Act
            var currentSeason = service.CurrentSeason();

            // Assert
            Assert.Equal(2019, currentSeason);
        }

        [Fact]
        public void CurrentSeasonDetails_Passes()
        {
            // Arrange
            var mocker = new AutoMocker();

            var service = mocker.CreateInstance<CfbService>();

            // Act
            var currentSeasonDetails = service.CurrentSeasonDetails();

            // Assert
            Assert.NotNull(currentSeasonDetails);
        }

        [Fact]
        public void CurrentSeasonType_Passes()
        {
            // Arrange
            var mocker = new AutoMocker();

            var service = mocker.CreateInstance<CfbService>();

            // Act
            var currentSeasonType = service.CurrentSeasonType();

            // Assert
            Assert.Equal("\"REG\"", currentSeasonType);
        }

        [Fact]
        public void CurrentWeek_Passes()
        {
            // Arrange
            var mocker = new AutoMocker();

            var service = mocker.CreateInstance<CfbService>();

            // Act
            var currentSeason = service.CurrentWeek();

            // Assert
            Assert.Equal(1, currentSeason);
        }

        [Fact]
        public void Stadiums_Passes()
        {
            // Arrange
            var mocker = new AutoMocker();

            var service = mocker.CreateInstance<CfbService>();

            // Act
            var stadiums = service.Stadiums();

            // Assert
            Assert.NotNull(stadiums);
        }

        [Fact]
        public void Teams_Passes()
        {
            // Arrange
            var mocker = new AutoMocker();

            var service = mocker.CreateInstance<CfbService>();

            // Act
            var teams = service.Teams();

            // Assert
            Assert.NotNull(teams);
        }

        [Fact]
        public void GamesByWeek_Passes()
        {
            // Arrange
            var mocker = new AutoMocker();

            var service = mocker.CreateInstance<CfbService>();

            // Act
            var games = service.GamesByWeek(2018, 1);

            // Assert
            Assert.NotNull(games);
        }

        [Fact]
        public void Schedules_Passes()
        {
            // Arrange
            var mocker = new AutoMocker();

            var service = mocker.CreateInstance<CfbService>();

            // Act
            var schedules = service.Schedules(2018);

            // Assert
            Assert.NotNull(schedules);
        }

        [Fact]
        public async Task ScoreboardAsync_Passes()
        {
            // Arrange
            var mocker = new AutoMocker();

            var service = mocker.CreateInstance<CfbService>();

            // Act
            var scoreboard = await service.ScoreboardAsync(20150909);

            // Assert
            Assert.NotNull(scoreboard);
        }

        [Fact]
        public void GameSummary_Passes()
        {
            // Arrange
            var mocker = new AutoMocker();

            var service = mocker.CreateInstance<CfbService>();

            // Act
            var gameSummary = service.GameSummary(400934572);

            // Assert
            Assert.NotNull(gameSummary);
        }

        [Fact]
        public void TeamInformation_byTeamAbbreviation_Passes()
        {
            // Arrange
            var mocker = new AutoMocker();

            var service = mocker.CreateInstance<CfbService>();

            // Act
            var teamInformation = service.TeamInformation("gt");

            // Assert
            Assert.NotNull(teamInformation);
        }

        //[Fact]
        //public void TeamInformation_byTeamId_Passes()
        //{
        //    // Arrange
        //    var mocker = new AutoMocker();

        //    var service = mocker.CreateInstance<CfbService>();

        //    // Act
        //    var teamInformation = service.TeamInformation(32);

        //    // Assert
        //    Assert.NotNull(teamInformation);
        //}

        [Fact]
        public void WeeksPasses()
        {
            // Arrange
            var mocker = new AutoMocker();

            var service = mocker.CreateInstance<CfbService>();

            // Act
            var weeks = service.Weeks(2019, 2);

            // Assert
            Assert.NotNull(weeks);

            weeks.items.ForEach(Assert.NotNull);
        }

        [Fact]
        public void EventPasses()
        {
            // Arrange
            var mocker = new AutoMocker();

            var service = mocker.CreateInstance<CfbService>();

            // Act
            var gameEvent = service.Event(400934572);

            // Assert
            Assert.NotNull(gameEvent);
        }

        [Fact]
        public void Season_Passes()
        {
            // Arrange
            var mocker = new AutoMocker();

            var service = mocker.CreateInstance<CfbService>();

            // Act
            var season = service.Season(2018);

            // Assert
            Assert.NotNull(season);
        }

        //[Fact]
        //public void EspnTeams_Passes()
        //{
        //    // Arrange
        //    var mocker = new AutoMocker();

        //    var service = mocker.CreateInstance<CfbService>();

        //    // Act
        //    var teams = service.EspnTeams(2019);

        //    // Assert
        //    Assert.NotNull(teams);
        //}

        [Fact]
        public void EspnTeam_Passes()
        {
            // Arrange
            var mocker = new AutoMocker();

            var service = mocker.CreateInstance<CfbService>();

            // Act
            var team = service.EspnTeam(2019, 32);

            // Assert
            Assert.NotNull(team);
        }

        //[Fact]
        //public void Venues_Passes()
        //{
        //    // Arrange
        //    var mocker = new AutoMocker();

        //    var service = mocker.CreateInstance<CfbService>();

        //    // Act
        //    var venues = service.Venues();

        //    // Assert
        //    Assert.NotNull(venues);
        //}

        //[Fact]
        //public void Venue_Passes()
        //{
        //    // Arrange
        //    var mocker = new AutoMocker();

        //    var service = mocker.CreateInstance<CfbService>();

        //    // Act
        //    var venue = service.Venue(3958);

        //    // Assert
        //    Assert.NotNull(venue);
        //}

        //[Fact]
        //public void Franchises_Passes()
        //{
        //    // Arrange
        //    var mocker = new AutoMocker();

        //    var service = mocker.CreateInstance<CfbService>();

        //    // Act
        //    var franchises = service.Franchises();

        //    // Assert
        //    Assert.NotNull(franchises);
        //}

        //[Fact]
        //public void Franchise_Passes()
        //{
        //    // Arrange
        //    var mocker = new AutoMocker();

        //    var service = mocker.CreateInstance<CfbService>();

        //    // Act
        //    var franchise = service.Franchise(99);

        //    // Assert
        //    Assert.NotNull(franchise);
        //}

        //[Fact]
        //public void Awards_Passes()
        //{
        //    // Arrange
        //    var mocker = new AutoMocker();

        //    var service = mocker.CreateInstance<CfbService>();

        //    // Act
        //    var awards = service.Awards(99);

        //    // Assert
        //    Assert.NotNull(awards);
        //}

        [Fact]
        public void Award_Passes()
        {
            // Arrange
            var mocker = new AutoMocker();

            var service = mocker.CreateInstance<CfbService>();

            // Act
            var award = service.Award("http://sports.core.api.espn.com/v2/sports/football/leagues/college-football/seasons/2018/awards/4?lang=en");

            // Assert
            Assert.NotNull(award);
        }
    }
}