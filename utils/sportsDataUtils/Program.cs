﻿
using System;
using System.Configuration;
using System.Threading.Tasks;
using EasyConsole;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace sportsDataUtils
{
    class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = new HostBuilder()
                .ConfigureAppConfiguration((hostContext, config) =>
                {
                    //config.AddJsonFile("appsettings.json", optional: true);
                    config.AddEnvironmentVariables();
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddOptions();
                    //services.Configure<AppSettingsReader>(hostContext.Configuration.GetSection("AppConfig"));
                    services.AddLogging(c =>
                    {
                        c.AddConsole();
                        //c.AddFilter("Microsoft.EntityFrameworkCore.Database.Command", LogLevel.Warning);
                    });

                    //services.AddHostedService<EventDispatcherService>();
                    //services.AddDbContext<JobDataContext>(options =>
                    //{
                    //    options.EnableDetailedErrors();

                    //    const string localMigrationsDatabaseConnection = "Server=localhost;Port=3306;Database=JobService;Uid=root;Pwd=Password1;SSLMode=None";
                    //    options.UseMySql(localMigrationsDatabaseConnection);
                    //});
                    //services.AddSingleton<ISnsPublisher, SnsPublisher>();
                });

            //await builder.RunConsoleAsync();

            object input = Input.ReadString("Please enter a string:");
            Output.WriteLine("You wrote: {0}", input);

            input = Input.ReadInt("Please enter an integer (between 1 and 10):", min: 1, max: 10);
            Output.WriteLine("You wrote: {0}", input);

            Console.WriteLine("<Press any key to exit>");
            input = Console.ReadLine();
        }
    }
}
