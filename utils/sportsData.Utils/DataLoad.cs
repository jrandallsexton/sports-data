﻿using System.Collections.Generic;

using Xunit;
using Xunit.Sdk;

namespace sportsData.Utils
{

    public class DataLoad
    {
        private readonly TestOutputHelper _outputHelper = new TestOutputHelper();

        [Fact]
        public void LoadStates()
        {
            var svc = new CfbService();
            var stadiums = svc.Stadiums();

            var states = new List<string>();

            stadiums.ForEach(s =>
            {
                if (!string.IsNullOrEmpty(s.State) && !states.Contains(s.State))
                    states.Add(s.State);
            });

            states.Sort();

            states.ForEach(s =>
            {
                _outputHelper.WriteLine(s);
            });
        }

        [Fact]
        public void LoadCities()
        {
            var svc = new CfbService();
            var stadiums = svc.Stadiums();

            var values = new List<string>();

            stadiums.ForEach(s =>
            {
                if (!values.Contains(s.City))
                    values.Add(s.City);
            });

            values.Sort();

            values.ForEach(s =>
            {
                _outputHelper.WriteLine(s);
            });
        }
    }
}