﻿
using FluentAssertions;

using sportsData.Common.ExtensionMethods;
using sportsData.Infrastructure.Espn;

using System;
using System.IO;
using System.Threading.Tasks;

using Xunit;

namespace sportsData.Utils
{

    public class EspnHarvester
    {
        [Fact]
        public void GetScoreboardDataFromEspnAndPersistToLocalJson()
        {

            var startYear = 2019;
            const int endYear = 2019;
            var commitMessage = "scoreboards for: ";

            while (startYear >= endYear)
            {
                var startDate = new DateTime(startYear, 08, 01);
                var endDate = new DateTime(startYear+1, 01, 31);
                commitMessage += $"{startYear}-{startYear + 1}; ";
                var cfbService = new CfbService();

                while (startDate <= endDate)
                {
                    var dateString = startDate.ToString("yyyy/MM/dd");
                    dateString = dateString.Replace("/", string.Empty);

                    var dateParameter = int.Parse(dateString);

                    var retry = true;
                    var retryCount = 0;
                    const int retryMax = 5;

                    while (retry)
                    {
                        try
                        {
                            var scoreboard = cfbService.Scoreboard(dateParameter, false);
                            retry = false;
                        }
                        catch (Exception ex)
                        {
                            retryCount++;
                            Console.WriteLine($"Failed: {dateParameter} => Retrying.  Count: {retryCount}");
                            retry = (retryCount < retryMax) ? true : false;
                            System.Threading.Thread.Sleep(2500); // pause 7.5 seconds so we don't get blacklisted from the ESPN api
                            if (retryCount == 4)
                            {
                                WriteLog("Scoreboard-Failure-Dates.txt", ex.ToString());
                                WriteLog("Scoreboard-Failure-Dates.txt", dateParameter.ToString());
                                Console.WriteLine($"*** FAILURE in {dateParameter.ToString()} ***");
                            }
                        }
                    }

                    startDate = startDate.AddDays(1);

                    System.Threading.Thread.Sleep(2500); // pause 7.5 seconds so we don't get blacklisted from the ESPN api
                }

                startYear -= 1;
            }

            Console.WriteLine(commitMessage);
        }

        [Fact]
        public void GetGameSummaryFromScoreboardFromEspnAndPersistToLocalJson()
        {
            var initialStartDate = new DateTime(2019, 08, 01);
            var startDate = initialStartDate;
            var endDate = new DateTime(2020, 01, 31);

            var cfbService = new CfbService();

            var gameSummariesSaved = 0;

            while (startDate <= endDate)
            {
                var dateString = startDate.ToString("yyyy/MM/dd");
                dateString = dateString.Replace("/", string.Empty);

                var dateParameter = int.Parse(dateString);

                var scoreboard = cfbService.Scoreboard(dateParameter);

                foreach (var game in scoreboard.events)
                {
                    var gameId = int.Parse(game.id);

                    var retry = true;
                    var retryCount = 0;

                    while (retry)
                    {
                        try
                        {
                            var gameSummary = cfbService.GameSummary(gameId);
                            retry = false;
                            gameSummariesSaved += 1;
                        }
                        catch (Exception ex)
                        {
                            retryCount++;
                            Console.WriteLine($"Failed: {gameId} => Retrying.  Count: {retryCount}");
                            retry = true;
                            System.Threading.Thread.Sleep(2000); // pause 7.5 seconds so we don't get blacklisted from the ESPN api
                            if (retryCount == 5)
                            {
                                WriteLog("GameSummary-Failure-Dates.txt", ex.ToString());
                                WriteLog("GameSummary-Failure-Dates.txt", gameId.ToString());
                                retry = false;
                            }
                        }
                    }
                }

                startDate = startDate.AddDays(1);

                System.Threading.Thread.Sleep(2000); // pause 7.5 seconds so we don't get blacklisted from the ESPN api
            }

            Console.WriteLine($"{gameSummariesSaved} Game Summaries: {initialStartDate.ToString("yyyy/MM/dd")}-{endDate.ToString("yyyy/MM/dd")}");
        }

        [Fact]
        public async Task GetTeamsBySeasonAndPersistToLocalJson()
        {
            const int season = 2020;

            var espnApiClient = new EspnApiClient(null, GetConfig());

            var teams = await espnApiClient.Teams(season);

            var mask0 = $"http://sports.core.api.espn.com/v2/sports/football/leagues/college-football/seasons/{season}/teams/";
            const string mask1 = "?lang=en";

            foreach (var item in teams.items)
            {
                Console.WriteLine($"Processing: {item.href}");
                var url = item.href;
                url = url.Replace(mask0, string.Empty);
                url = url.Replace(mask1, string.Empty);

                var parsed = int.TryParse(url, out var teamId);

                var retry = true;
                var idx = 0;

                var teamInformationLoaded = false;
                var teamDataLoaded = false;

                const int sleepDuration = 2000;

                while (retry)
                {
                    try
                    {
                        var teamInformation = await espnApiClient.TeamInformation(teamId);
                        teamInformationLoaded = true;
                        retry = false;
                    }
                    catch (Exception ex)
                    {
                        System.Threading.Thread.Sleep(sleepDuration); // pause so we don't get blacklisted from the ESPN api
                        idx++;
                        retry = idx < 5;
                    }
                }

                System.Threading.Thread.Sleep(sleepDuration); // pause so we don't get blacklisted from the ESPN api

                retry = true;

                while (retry)
                {
                    try
                    {
                        var teamData = await espnApiClient.EspnTeam(season, teamId);
                        teamDataLoaded = true;
                        retry = false;
                    }
                    catch (Exception ex)
                    {
                        System.Threading.Thread.Sleep(sleepDuration); // pause so we don't get blacklisted from the ESPN api
                        idx++;
                        retry = idx < 5;
                    }
                }

                if (!teamInformationLoaded || !teamDataLoaded)
                    Console.WriteLine($"*** FAILURE ***\t{teamId}\t{teamInformationLoaded}\t{teamDataLoaded}");
            }
        }

        [Fact]
        public async Task GetVenuesAndPersistToLocalJson()
        {
            var espnApiClient = new EspnApiClient(null, GetConfig());

            var venues = await espnApiClient.Venues();

            await venues.items.ForEachAsync(async item =>
            {
                var venue = await espnApiClient.Venue(item.id, true);
                venue.Should().NotBeNull();

                await venue.Images.ForEachAsync(async i =>
                {
                    await espnApiClient.GetMedia(i.Href.AbsoluteUri);
                    await Task.Delay(1500);
                });
            });
        }

        [Fact]
        public async Task GetFranchisesAndPersistToLocalJson()
        {
            var espnApiClient = new EspnApiClient(null, GetConfig());

            var franchises = await espnApiClient.Franchises();

            await franchises.items.ForEachAsync(async item =>
            {
                var franchise = await espnApiClient.Franchise(item.id);
                franchise.Should().NotBeNull();
            });
        }

        [Fact]
        public async Task GetFranchiseAwards()
        {
            var espnApiClient = new EspnApiClient(null, GetConfig());

            var franchises = await espnApiClient.Franchises();

            await franchises.items.ForEachAsync(async item =>
            {
                var awards = await espnApiClient.AwardsByFranchise(item.id);
                awards.Should().NotBeNull();
                await Task.Delay(1000);
            });
        }

        private static void WriteLog(string file, string log)
        {
            var path = @"C:\Projects\bitbucket\sports-data\logs";
            path = Path.Combine(path, file);

            // This text is added only once to the file.
            if (!File.Exists(path))
            {
                // Create a file to write to.
                using var streamWriter = File.CreateText(path);
                streamWriter.WriteLine(log);
                return;
            }

            // This text is always added, making the file longer over time
            // if it is not deleted.
            using (var streamWriter = File.AppendText(path))
                streamWriter.WriteLine(log);
        }

        private static EspnApiClientConfig GetConfig()
        {
            // TODO: Move this to appsettings.js
            return new EspnApiClientConfig()
            {
                DataDirectory = @"C:\Dropbox\Code\sports-data\data",
                MediaDirectory = @"C:\Dropbox\Code\sports-data\media"
            };
        }
    }
}