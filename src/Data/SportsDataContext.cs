﻿using Microsoft.EntityFrameworkCore;

using sportsData.Common.Eventing;
using sportsData.Common.Eventing.Providers;

namespace sportsData.Data
{
    public class SportsDataContext : DbContext, IProvideEventingData
    {
        public SportsDataContext(DbContextOptions<SportsDataContext> options)
            : base(options) { }

        public DbSet<OutgoingEvent> OutgoingEvents { get; set; }
        public DbSet<IncomingEvent> IncomingEvents { get; set; }
    }
}