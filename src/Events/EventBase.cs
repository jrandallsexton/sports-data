﻿using sportsData.Common.Eventing;

using System;

namespace sportsData.Events
{
    public abstract class EventBase
    {
        protected EventBase(Guid correlationId, Guid causationId, EventingMethod eventingMethod)
        {
            CorrelationId = correlationId;
            CausationId = causationId;
            EventMethod = eventingMethod;
        }

        public Guid CorrelationId { get; set; }

        public Guid CausationId { get; set; }

        public EventingMethod EventMethod { get; set; }

        public DateTime CreatedUtc { get; set; } = DateTime.UtcNow;

        public int CreatedBy { get; set; }

        public string CreatedByName { get; set; }
    }
}