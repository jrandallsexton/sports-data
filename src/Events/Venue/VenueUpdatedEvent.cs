﻿using sportsData.Common.Eventing;

using System;

namespace sportsData.Events.Venue
{
    public class VenueUpdatedEvent : VenueEventBase
    {
        public VenueUpdatedEvent(Guid correlationId, Guid causationId)
            : base(correlationId, causationId, EventingMethod.Updated) { }
    }
}