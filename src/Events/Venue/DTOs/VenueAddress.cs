﻿namespace sportsData.Events.Venue.DTOs
{
    public class VenueAddress
    {
        public string City { get; set; }
        public string State { get; set; }
        public long Zip { get; set; }
    }
}