﻿using System;
using System.Collections.Generic;

namespace sportsData.Events.Venue.DTOs
{
    public class VenueImage
    {
        public Uri Href { get; set; }
        public long Width { get; set; }
        public long Height { get; set; }
        public string Alt { get; set; }
        public List<string> Rel { get; set; }
    }
}