﻿using System.Collections.Generic;

namespace sportsData.Events.Venue.DTOs
{
    public class Venue
    {
        public string Id { get; set; }

        public string FullName { get; set; }

        public string ShortName { get; set; }

        public long Capacity { get; set; }

        public bool IsGrass { get; set; }

        public bool IsIndoor { get; set; }

        public VenueAddress Address { get; set; }

        public List<VenueImage> Images { get; set; }
    }
}