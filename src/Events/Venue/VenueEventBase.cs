﻿using sportsData.Common.Eventing;
using sportsData.Events.Venue.DTOs;

using System;
using System.Collections.Generic;

namespace sportsData.Events.Venue
{
    public abstract class VenueEventBase : EventBase
    {
        protected VenueEventBase(Guid correlationId, Guid causationId, EventingMethod eventingMethod) :
            base(correlationId, causationId, eventingMethod) { }

        public DTOs.Venue Venue { get; set; }
    }
}