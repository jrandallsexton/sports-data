﻿using sportsData.Common.Eventing;

using System;

namespace sportsData.Events.Venue
{
    public class VenueCreatedEvent : VenueEventBase
    {
        public VenueCreatedEvent(Guid correlationId, Guid causationId)
            : base(correlationId, causationId, EventingMethod.Created) { }
    }
}