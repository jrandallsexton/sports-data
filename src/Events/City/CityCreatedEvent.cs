﻿using sportsData.Common.Eventing;

using System;

namespace sportsData.Events.City
{
    public class CityCreatedEvent : EventBase
    {
        public CityCreatedEvent(Guid correlationId, Guid causationId)
            : base(correlationId, causationId, EventingMethod.Created) { }
    }
}