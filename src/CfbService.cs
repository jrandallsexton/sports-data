﻿
using Newtonsoft.Json;

using sportsData.Infrastructure.Espn.Dtos.Award;
using sportsData.Infrastructure.Espn.Dtos.GameSummary;
using sportsData.Infrastructure.Espn.Dtos.Scoreboard;
using sportsData.Infrastructure.Espn.Dtos.TeamInformation;
using sportsData.Infrastructure.Espn.Dtos.Weeks;
using sportsData.Infrastructure.SportsDataIo.Dtos;

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;

using Season = sportsData.Infrastructure.Espn.Dtos.GameSummary.Season;
using Team = sportsData.Infrastructure.SportsDataIo.Dtos.Team;

namespace sportsData
{
    public class CfbService : ICfbService
    {
        private bool persistJsonToDisk = true;

        public bool AreAnyGamesInProgress()
        {
            var uri = new Uri("https://api.sportsdata.io/v3/cfb/scores/json/AreAnyGamesInProgress");

            var result = PerformHttpGet(uri);

            return bool.Parse(result);
        }

        public List<Conference> LeagueHierarchy()
        {
            var uri = new Uri("https://api.sportsdata.io/v3/cfb/scores/json/LeagueHierarchy");
            var result = PerformHttpGet(uri);

            var data = JsonConvert.DeserializeObject<List<Conference>>(result);

            return data;
        }

        public int CurrentSeason()
        {
            var uri = new Uri("https://api.sportsdata.io/v3/cfb/scores/json/CurrentSeason");

            var result = PerformHttpGet(uri);

            return int.Parse(result);
        }

        public CurrentSeasonDetails CurrentSeasonDetails()
        {
            var uri = new Uri("https://api.sportsdata.io/v3/cfb/scores/json/CurrentSeasonDetails");
            var result = PerformHttpGet(uri);

            var data = JsonConvert.DeserializeObject<CurrentSeasonDetails>(result);

            return data;
        }

        public string CurrentSeasonType()
        {
            var uri = new Uri("https://api.sportsdata.io/v3/cfb/scores/json/CurrentSeasonType");

            var result = PerformHttpGet(uri);

            return result.Trim();
        }

        public int CurrentWeek()
        {
            var uri = new Uri("https://api.sportsdata.io/v3/cfb/scores/json/CurrentWeek");

            var result = PerformHttpGet(uri);

            return int.Parse(result);
        }

        public List<Stadium> Stadiums()
        {
            var uri = new Uri("https://api.sportsdata.io/v3/cfb/scores/json/Stadiums");
            var result = PerformHttpGet(uri);

            var data = JsonConvert.DeserializeObject<List<Stadium>>(result);

            return data;
        }

        public List<Team> Teams()
        {
            var uri = new Uri("https://api.sportsdata.io/v3/cfb/scores/json/Teams");
            var result = PerformHttpGet(uri);

            var data = JsonConvert.DeserializeObject<List<Team>>(result);

            return data;
        }

        public List<Game> GamesByWeek(int season, int week)
        {
            var uri = new Uri($"https://api.sportsdata.io/v3/cfb/scores/json/GamesByWeek/{season}/{week}");
            var result = PerformHttpGet(uri);

            var data = JsonConvert.DeserializeObject<List<Game>>(result);

            return data;
        }

        public List<ScheduledGame> Schedules(int season)
        {
            var uri = new Uri($"https://api.sportsdata.io/v3/cfb/scores/json/Games/{season}");
            var result = PerformHttpGet(uri);

            var data = JsonConvert.DeserializeObject<List<ScheduledGame>>(result);

            return data;
        }

        public Scoreboard Scoreboard(int yyyymmdd, bool useCache = true)
        {
            var uri = new Uri($"http://site.api.espn.com/apis/site/v2/sports/football/college-football/scoreboard?dates={yyyymmdd}");
            var result = PerformHttpGet(uri, true, useCache);

            var data = JsonConvert.DeserializeObject<Scoreboard>(result);

            return data;
        }

        public async Task<Scoreboard> ScoreboardAsync(int yyyymmdd)
        {
            var uri = new Uri($"http://site.api.espn.com/apis/site/v2/sports/football/college-football/scoreboard?dates={yyyymmdd}");
            var result = await PerformHttpGetAsync(uri, true);

            var data = JsonConvert.DeserializeObject<Scoreboard>(result);

            return data;
        }

        public GameSummary GameSummary(int espnGameId)
        {
            var uri = new Uri($"http://site.api.espn.com/apis/site/v2/sports/football/college-football/summary?event={espnGameId}");
            var result = PerformHttpGet(uri, true);

            var data = JsonConvert.DeserializeObject<GameSummary>(result);

            return data;
        }

        public TeamInformation TeamInformation(string espnTeamAbbreviation)
        {
            var uri = new Uri($"http://site.api.espn.com/apis/site/v2/sports/football/college-football/teams/{espnTeamAbbreviation}");
            var result = PerformHttpGet(uri, true);

            var data = JsonConvert.DeserializeObject<TeamInformation>(result);

            return data;
        }

        //public TeamInformation TeamInformation(int espnTeamId)
        //{
        //    var uri = new Uri($"http://site.api.espn.com/apis/site/v2/sports/football/college-football/teams/{espnTeamId}");
        //    var result = PerformHttpGet(uri, true);

        //    var data = JsonConvert.DeserializeObject<TeamInformation>(result);

        //    return data;
        //}

        public Weeks Weeks(int season, int seasonType)
        {
            var uri = new Uri($"http://sports.core.api.espn.com/v2/sports/football/leagues/college-football/seasons/{season}/types/{seasonType}/weeks?lang=en");
            var result = PerformHttpGet(uri, true);
            result = result.Replace("$ref", "href");
            var data = JsonConvert.DeserializeObject<Weeks>(result);

            return data;
        }

        public Infrastructure.Espn.Dtos.Team.Team EspnTeam(int year, int teamId)
        {
            var uri = new Uri($"http://sports.core.api.espn.com/v2/sports/football/leagues/college-football/seasons/{year}/teams/{teamId}?lang=en");
            var result = PerformHttpGet(uri, true);
            //result = result.Replace("$ref", "href");

            var settings = new JsonSerializerSettings
            {
                MetadataPropertyHandling = MetadataPropertyHandling.Ignore
            };
            var data = JsonConvert.DeserializeObject<Infrastructure.Espn.Dtos.Team.Team>(result, settings);

            return data;
        }

        public Event Event(int eventId)
        {
            var uri = new Uri($"http://sports.core.api.espn.com/v2/sports/football/leagues/college-football/events/{eventId}?lang=en");
            var result = PerformHttpGet(uri, true);
            //result = result.Replace("$ref", "href");

            var settings = new JsonSerializerSettings
            {
                MetadataPropertyHandling = MetadataPropertyHandling.Ignore
            };
            var data = JsonConvert.DeserializeObject<Event>(result, settings);

            return data;
        }

        //public ResourceIndex Awards(int franchiseId)
        //{
        //    var uri = new Uri($"http://sports.core.api.espn.com/v2/sports/football/leagues/college-football/franchises/{franchiseId}/awards?lang=en&limit=999");
        //    var result = PerformHttpGet(uri, true);

        //    result = result.Replace("$ref", "href");
        //    var settings = new JsonSerializerSettings
        //    {
        //        MetadataPropertyHandling = MetadataPropertyHandling.Ignore
        //    };
        //    var data = JsonConvert.DeserializeObject<ResourceIndex>(result, settings);

        //    return data;
        //}

        public Award Award(string url)
        {
            var uri = new Uri(url);
            var result = PerformHttpGet(uri, true);

            var settings = new JsonSerializerSettings
            {
                MetadataPropertyHandling = MetadataPropertyHandling.Ignore
            };
            var data = JsonConvert.DeserializeObject<Award>(result, settings);

            return data;
        }

        public Season Season(int year)
        {
            var uri = new Uri($"http://sports.core.api.espn.com/v2/sports/football/leagues/college-football/seasons/{year}?lang=en");
            var result = PerformHttpGet(uri, true);

            //result = result.Replace("$ref", "href");
            var settings = new JsonSerializerSettings
            {
                MetadataPropertyHandling = MetadataPropertyHandling.Ignore
            };
            var data = JsonConvert.DeserializeObject<Season>(result, settings);

            return data;
        }

        private async Task<string> PerformHttpGetAsync(Uri uri, bool isEspn = false)
        {
            var filename = GenerateFileNameFromUri(uri);

            var result = GetJsonFromFile(filename);
            if (!string.IsNullOrEmpty(result))
                return result;

            try
            {
                using (var client = new WebClient())
                {
                    if (!isEspn)
                        client.Headers.Add("Ocp-Apim-Subscription-Key", "a5a95819900d4267a3494cc3b893e5fe");

                    result = await client.DownloadStringTaskAsync(uri);

                    if (persistJsonToDisk)
                        PersistJsonToDisk(filename, result);

                    return result;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw;
            }
        }

        public string PerformHttpGet(Uri uri, bool isEspn = false, bool useCached = true)
        {
            var filename = GenerateFileNameFromUri(uri);

            string result;

            if (useCached)
            {
                result = GetJsonFromFile(filename);
                if (!string.IsNullOrEmpty(result))
                    return result;
            }

            try
            {
                using (var client = new WebClient())
                {
                    if (!isEspn)
                        client.Headers.Add("Ocp-Apim-Subscription-Key", "a5a95819900d4267a3494cc3b893e5fe");

                    using (var stream = client.OpenRead(uri))
                    {
                        if (stream == null) return string.Empty;
                        using (var streamReader = new StreamReader(stream))
                            result = streamReader.ReadToEnd();

                        if (persistJsonToDisk)
                            PersistJsonToDisk(filename, result);

                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw;
            }
        }

        private string GetJsonFromFile(string filename)
        {
            var path = Path.Combine(@"C:\Projects\bitbucket\sports-data\data", filename);

            if (!File.Exists(path))
                return string.Empty;

            return File.ReadAllText(path);
        }

        private void PersistJsonToDisk(string filename, string jsonData)
        {
            var path = Path.Combine(@"C:\Projects\bitbucket\sports-data\data", filename);
            System.IO.File.WriteAllText(path, jsonData);
        }

        private string GenerateFileNameFromUri(Uri uri)
        {
            var fileName = uri.AbsoluteUri;
            fileName = fileName.Replace("https://", string.Empty);
            fileName = fileName.Replace("http://", string.Empty);
            fileName = fileName.Replace("/", "-");
            fileName = fileName.Replace("?", "-");
            fileName = fileName.Replace("=", "-");
            fileName += ".json";

            return fileName;
        }
    }
}