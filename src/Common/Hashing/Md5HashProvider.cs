﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace sportsData.Common.Hashing
{
    public interface IProvideHashes
    {
        string GenerateHash(string input);
    }

    public class Md5HashProvider : IProvideHashes
    {
        public string GenerateHash(string input)
        {
            // Source: https://stackoverflow.com/questions/11454004/calculate-a-md5-hash-from-a-string
            
            // byte array representation of that string
            var encodedBytes = new UTF8Encoding().GetBytes(input);

            // need MD5 to calculate the hash
            var hashBytes = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(encodedBytes);

            // string representation (similar to UNIX format)
            return BitConverter.ToString(hashBytes)
                // without dashes
                .Replace("-", string.Empty)
                // make lowercase
                .ToLower();
        }
    }
}