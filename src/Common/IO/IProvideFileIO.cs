﻿using System.IO;
using System.Threading.Tasks;

namespace sportsData.Common.IO
{
    public interface IProvideFileIo
    {
        void DeleteFile(string path);
        Task<string> ReadAllTextAsync(string path);
        string[] GetFiles(string path, SearchOption searchOption);
    }

    public class FileIoProvider : IProvideFileIo
    {
        public void DeleteFile(string path)
        {
            File.Delete(path);
        }

        public async Task<string> ReadAllTextAsync(string path)
        {
            return await File.ReadAllTextAsync(path);
        }

        public string[] GetFiles(string path, SearchOption searchOption)
        {
            return Directory.GetFiles(path, string.Empty, searchOption);
        }
    }
}