﻿using Microsoft.EntityFrameworkCore;

using System;
using System.Threading;
using System.Threading.Tasks;

namespace sportsData.Common.Eventing.Providers
{
    public interface IProvideEventingData : IDisposable
    {
        DbSet<OutgoingEvent> OutgoingEvents { get; set; }
        DbSet<IncomingEvent> IncomingEvents { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}