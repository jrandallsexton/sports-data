﻿using sportsData.Events;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace sportsData.Common.Eventing.Consumers.Receivers
{
    public interface IReceiveEvents
    {
        Task<List<EventingBase>> Receive();
        Task Delete(Guid eventId);
    }
}