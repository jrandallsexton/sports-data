﻿using sportsData.Common.ExtensionMethods;
using sportsData.Common.IO;

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace sportsData.Common.Eventing.Consumers.Receivers
{
    public class FileBasedReceiver : IReceiveEvents
    {
        private readonly string _consumeDirectory;
        private readonly IProvideFileIo _fileIoProvider;

        public FileBasedReceiver(string consumeDirectory, IProvideFileIo fileIoProvider)
        {
            _consumeDirectory = consumeDirectory;
            _fileIoProvider = fileIoProvider;
        }

        public async Task<List<EventingBase>> Receive()
        {
            var incomingEvents = new List<EventingBase>();

            foreach (var fileName in _fileIoProvider.GetFiles(_consumeDirectory, SearchOption.AllDirectories))
            {
                var fileContent = await _fileIoProvider.ReadAllTextAsync(Path.Combine(_consumeDirectory, fileName));
                incomingEvents.Add(fileContent.FromJson<EventingBase>());
            }

            return incomingEvents;
        }

        public Task Delete(Guid eventId)
        {
            foreach (var fileInfo in new DirectoryInfo(_consumeDirectory).GetFiles())
            {
                if (fileInfo.Name.Contains(eventId.ToString()))
                {
                    _fileIoProvider.DeleteFile(fileInfo.FullName);
                }
            }
            return Task.CompletedTask;
        }
    }
}