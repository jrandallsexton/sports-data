﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace sportsData.Common.Eventing.Consumers
{
    public interface IProvideEventHandlers
    {
        Dictionary<string, Func<string, Task>> GetEventHandlers();
    }
}