﻿using System.Threading;
using System.Threading.Tasks;

namespace sportsData.Common.Eventing.Consumers
{
    public interface IConsumeEvents
    {
        Task ConsumeAsync(CancellationToken cancellationToken);
    }
}