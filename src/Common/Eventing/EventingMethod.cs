﻿namespace sportsData.Common.Eventing
{
    public enum EventingMethod
    {
        Created = 1,
        Updated = 2,
        Deleted = 3
    }
}