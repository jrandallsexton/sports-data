﻿using System.Threading.Tasks;

namespace sportsData.Common.Eventing.Publishers.Broadcasters
{
    public interface IBroadcastEvents
    {
        Task Broadcast<T>(T outgoingEvent) where T : EventingBase;
    }
}