﻿using Microsoft.Extensions.Logging;

using sportsData.Common.ExtensionMethods;

using System.IO;
using System.Threading.Tasks;

namespace sportsData.Common.Eventing.Publishers.Broadcasters
{
    public class FileBasedBroadcaster : IBroadcastEvents
    {
        private readonly ILogger<FileBasedBroadcaster> _logger;
        private readonly string _broadcastDirectory;

        public FileBasedBroadcaster(ILogger<FileBasedBroadcaster> logger, string broadcastDirectory)
        {
            _logger = logger;
            _broadcastDirectory = broadcastDirectory;
        }

        public async Task Broadcast<T>(T outgoingEvent) where T : EventingBase
        {
            _logger?.LogInformation("Begin broadcast with {@outgoingEvent}", outgoingEvent);

            if (!Directory.Exists(Path.Combine(_broadcastDirectory, outgoingEvent.EventType)))
                Directory.CreateDirectory(Path.Combine(_broadcastDirectory, outgoingEvent.EventType));

            var jsonData = outgoingEvent.ToJson();
            var path = Path.Combine(_broadcastDirectory, outgoingEvent.EventType, $"{outgoingEvent.Id}.event");

            await File.WriteAllTextAsync(path, jsonData);
        }
    }
}