﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using sportsData.Common.Eventing.Publishers.Broadcasters;
using sportsData.Common.ExtensionMethods;

using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using sportsData.Common.Eventing.Providers;

namespace sportsData.Common.Eventing.Publishers
{
    public class EventPublisher : IPublishEvents
    {
        private readonly ILogger<EventPublisher> _logger;
        private readonly IProvideEventingData _eventingData;
        private readonly IBroadcastEvents _eventBroadcaster;

        public EventPublisher(ILogger<EventPublisher> logger,
            IProvideEventingData eventingData,
            IBroadcastEvents eventBroadcaster)
        {
            _logger = logger;
            _eventingData = eventingData;
            _eventBroadcaster = eventBroadcaster;
        }

        public async Task PublishAsync(CancellationToken cancellationToken)
        {
            _logger?.LogInformation("Begin publish");

            var events = await _eventingData.OutgoingEvents
                .Where(e => e.Raised == false && !e.LockedUtc.HasValue)
                .ToListAsync(cancellationToken);

            if (events.Count == 0)
            {
                _logger?.LogInformation("Nothing to publish. Existing.");
                return;
            }

            _logger?.LogInformation($"Publishing {events.Count} events.");

            await events.ForEachAsync(async e =>
            {
                e.LockedUtc = DateTime.UtcNow;
                await _eventingData.SaveChangesAsync(cancellationToken);
            });

            await events.ForEachAsync(async e =>
            {
                await _eventBroadcaster.Broadcast(e);
                e.Raised = true;
                e.RaisedUtc = DateTime.UtcNow;
                e.LockedUtc = null;
                await _eventingData.SaveChangesAsync(cancellationToken);
            });
        }
    }
}