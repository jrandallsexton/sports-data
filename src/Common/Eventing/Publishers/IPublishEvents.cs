﻿using System.Threading;
using System.Threading.Tasks;

namespace sportsData.Common.Eventing.Publishers
{
    public interface IPublishEvents
    {
        Task PublishAsync(CancellationToken cancellationToken);
    }
}