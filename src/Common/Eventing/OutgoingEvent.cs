﻿using System;

namespace sportsData.Common.Eventing
{
    public class OutgoingEvent : EventingBase
    {
        public bool Raised { get; set; }
        public DateTime? RaisedUtc { get; set; }
    }
}