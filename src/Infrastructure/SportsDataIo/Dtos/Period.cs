﻿
namespace sportsData.Infrastructure.SportsDataIo.Dtos
{
    public class Period
    {
        public int PeriodID { get; set; }
        public int GameID { get; set; }
        public int Number { get; set; }
        public string Name { get; set; }
        public int AwayScore { get; set; }
        public int HomeScore { get; set; }
    }
}