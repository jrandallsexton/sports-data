﻿
using System.Collections.Generic;

namespace sportsData.Infrastructure.SportsDataIo.Dtos
{
    public class Conference
    {
        public int ConferenceID { get; set; }
        public string Name { get; set; }
        public string ConferenceName { get; set; }
        public string DivisionName { get; set; }
        public List<Team> Teams { get; set; }
    }
}