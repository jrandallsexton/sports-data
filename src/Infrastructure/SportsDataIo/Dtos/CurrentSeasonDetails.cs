﻿using Newtonsoft.Json;

namespace sportsData.Infrastructure.SportsDataIo.Dtos
{
    public partial class CurrentSeasonDetails
    {
        [JsonProperty("Season")]
        public long Season { get; set; }

        [JsonProperty("StartYear")]
        public long StartYear { get; set; }

        [JsonProperty("EndYear")]
        public long EndYear { get; set; }

        [JsonProperty("Description")]
        public long Description { get; set; }

        [JsonProperty("ApiSeason")]
        public string ApiSeason { get; set; }

        [JsonProperty("ApiWeek")]
        public long ApiWeek { get; set; }
    }
}