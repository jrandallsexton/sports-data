﻿using System;
using System.Collections.Generic;

namespace sportsData.Infrastructure.SportsDataIo.Dtos
{
    public class ScheduledGame
    {
        public int GameID { get; set; }
        public int Season { get; set; }
        public int SeasonType { get; set; }
        public int Week { get; set; }
        public string Status { get; set; }
        public DateTime Day { get; set; }
        public DateTime? DateTime { get; set; }
        public string AwayTeam { get; set; }
        public string HomeTeam { get; set; }
        public int AwayTeamID { get; set; }
        public int HomeTeamID { get; set; }
        public string AwayTeamName { get; set; }
        public string HomeTeamName { get; set; }
        public int? AwayTeamScore { get; set; }
        public int? HomeTeamScore { get; set; }
        public string Period { get; set; }
        public object TimeRemainingMinutes { get; set; }
        public object TimeRemainingSeconds { get; set; }
        public double? PointSpread { get; set; }
        public double? OverUnder { get; set; }
        public int? AwayTeamMoneyLine { get; set; }
        public int? HomeTeamMoneyLine { get; set; }
        public DateTime Updated { get; set; }
        public DateTime Created { get; set; }
        public int GlobalGameID { get; set; }
        public int GlobalAwayTeamID { get; set; }
        public int GlobalHomeTeamID { get; set; }
        public int StadiumID { get; set; }
        public int? YardLine { get; set; }
        public string YardLineTerritory { get; set; }
        public int? Down { get; set; }
        public int? Distance { get; set; }
        public string Possession { get; set; }
        public bool IsClosed { get; set; }
        public DateTime? GameEndDateTime { get; set; }
        public string Title { get; set; }
        public Stadium Stadium { get; set; }
        public List<ScheduledPeriod> Periods { get; set; }
    }
}
