﻿
namespace sportsData.Infrastructure.SportsDataIo.Dtos
{
    public class Stadium
    {
        public int StadiumID { get; set; }
        public bool Active { get; set; }
        public string Name { get; set; }
        public bool Dome { get; set; }
        public string City { get; set; }
        public string State { get; set; }
    }
}