﻿namespace sportsData.Infrastructure
{
    public enum ResourceSource
    {
        Espn = 1,
        SportsIO = 2,
        Other = 3
    }
}