﻿namespace sportsData.Infrastructure.Espn
{
    public enum EspnResourceType
    {
        Athlete = 1,
        Award = 2,
        Event = 3,
        Franchise = 4,
        GameSummary = 5,
        Scoreboard = 6,
        Season = 7,
        Team = 8,
        TeamInformation = 9,
        Venue = 10,
        Week = 11,
        Unknown = 99
    }
}