﻿using System;
using System.Collections.Generic;

namespace sportsData.Infrastructure.Espn.Dtos.GameSummary
{
    public class Team2
    {
        public string id { get; set; }
        public string uid { get; set; }
        public string slug { get; set; }
        public string location { get; set; }
        public string name { get; set; }
        public string abbreviation { get; set; }
        public string displayName { get; set; }
        public string shortDisplayName { get; set; }
        public string color { get; set; }
        public string alternateColor { get; set; }
        public string logo { get; set; }
    }

    public class Statistic
    {
        public string name { get; set; }
        public string displayValue { get; set; }
        public string label { get; set; }
    }

    public class Team
    {
        public Team2 team { get; set; }
        public List<Statistic> statistics { get; set; }
    }

    public class Team3
    {
        public string id { get; set; }
        public string uid { get; set; }
        public string slug { get; set; }
        public string location { get; set; }
        public string name { get; set; }
        public string abbreviation { get; set; }
        public string displayName { get; set; }
        public string shortDisplayName { get; set; }
        public string color { get; set; }
        public string alternateColor { get; set; }
        public string logo { get; set; }
    }

    public class Statistic2
    {
        public string name { get; set; }
        public string text { get; set; }
        public List<string> labels { get; set; }
        public List<string> descriptions { get; set; }
        public List<object> athletes { get; set; }
        public List<object> totals { get; set; }
    }

    public class Player
    {
        public Team3 team { get; set; }
        public List<Statistic2> statistics { get; set; }
    }

    public class Boxscore
    {
        public List<Team> teams { get; set; }
        public List<Player> players { get; set; }
    }

    public class Address
    {
        public string city { get; set; }
        public string state { get; set; }
        public string zipCode { get; set; }
    }

    public class Image
    {
        public string href { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public string alt { get; set; }
        public List<string> rel { get; set; }
    }

    public class Venue
    {
        public string id { get; set; }
        public string fullName { get; set; }
        public string shortName { get; set; }
        public Address address { get; set; }
        public int capacity { get; set; }
        public bool grass { get; set; }
        public List<Image> images { get; set; }
    }

    public class GameInfo
    {
        public Venue venue { get; set; }
        public int attendance { get; set; }
    }

    public class Logo
    {
        public string href { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public string alt { get; set; }
        public List<string> rel { get; set; }
    }

    public class Team4
    {
        public string name { get; set; }
        public string abbreviation { get; set; }
        public string displayName { get; set; }
        public string shortDisplayName { get; set; }
        public List<Logo> logos { get; set; }
    }

    public class Period
    {
        public string type { get; set; }
        public int number { get; set; }
    }

    public class Clock
    {
        public string displayValue { get; set; }
    }

    public class Start
    {
        public Period period { get; set; }
        public Clock clock { get; set; }
        public int yardLine { get; set; }
        public string text { get; set; }
    }

    public class Period2
    {
        public string type { get; set; }
        public int number { get; set; }
    }

    public class Clock2
    {
        public string displayValue { get; set; }
    }

    public class End
    {
        public Period2 period { get; set; }
        public Clock2 clock { get; set; }
        public int yardLine { get; set; }
        public string text { get; set; }
    }

    public class TimeElapsed
    {
        public string displayValue { get; set; }
    }

    public class Type
    {
        public string id { get; set; }
    }

    public class Period3
    {
        public int number { get; set; }
    }

    public class Clock3
    {
        public string displayValue { get; set; }
    }

    public class Team5
    {
        public string id { get; set; }
    }

    public class Start2
    {
        public int down { get; set; }
        public int distance { get; set; }
        public int yardLine { get; set; }
        public int yardsToEndzone { get; set; }
        public Team5 team { get; set; }
        public string downDistanceText { get; set; }
        public string shortDownDistanceText { get; set; }
        public string possessionText { get; set; }
    }

    public class Team6
    {
        public string id { get; set; }
    }

    public class End2
    {
        public int down { get; set; }
        public int distance { get; set; }
        public int yardLine { get; set; }
        public int yardsToEndzone { get; set; }
        public string downDistanceText { get; set; }
        public string shortDownDistanceText { get; set; }
        public string possessionText { get; set; }
        public Team6 team { get; set; }
    }

    public class ScoringType
    {
        public string name { get; set; }
        public string displayName { get; set; }
        public string abbreviation { get; set; }
    }

    public class Play
    {
        public string id { get; set; }
        public Type type { get; set; }
        public string text { get; set; }
        public int awayScore { get; set; }
        public int homeScore { get; set; }
        public Period3 period { get; set; }
        public Clock3 clock { get; set; }
        public bool scoringPlay { get; set; }
        public bool priority { get; set; }
        public string modified { get; set; }
        public DateTime wallclock { get; set; }
        public Start2 start { get; set; }
        public End2 end { get; set; }
        public int statYardage { get; set; }
        public ScoringType scoringType { get; set; }
    }

    public class Previou
    {
        public string id { get; set; }
        public string description { get; set; }
        public Team4 team { get; set; }
        public Start start { get; set; }
        public End end { get; set; }
        public TimeElapsed timeElapsed { get; set; }
        public int yards { get; set; }
        public bool isScore { get; set; }
        public int offensivePlays { get; set; }
        public string result { get; set; }
        public string shortDisplayResult { get; set; }
        public string displayResult { get; set; }
        public List<Play> plays { get; set; }
    }

    public class Drives
    {
        public List<Previou> previous { get; set; }
    }

    public class Link
    {
        public string href { get; set; }
        public string text { get; set; }
    }

    public class Team7
    {
        public string id { get; set; }
        public string uid { get; set; }
        public string displayName { get; set; }
        public string abbreviation { get; set; }
        public List<Link> links { get; set; }
        public string logo { get; set; }
    }

    public class Link2
    {
        public List<string> rel { get; set; }
        public string href { get; set; }
        public string text { get; set; }
    }

    public class Headshot
    {
        public string href { get; set; }
        public string alt { get; set; }
    }

    public class Position
    {
        public string abbreviation { get; set; }
    }

    public class Athlete
    {
        public string id { get; set; }
        public string uid { get; set; }
        public string guid { get; set; }
        public string lastName { get; set; }
        public string fullName { get; set; }
        public string displayName { get; set; }
        public string shortName { get; set; }
        public List<Link2> links { get; set; }
        public string jersey { get; set; }
        public Headshot headshot { get; set; }
        public Position position { get; set; }
    }

    public class Leader3
    {
        public string displayValue { get; set; }
        public Athlete athlete { get; set; }
    }

    public class Leader2
    {
        public string name { get; set; }
        public string displayName { get; set; }
        public List<Leader3> leaders { get; set; }
    }

    public class Leader
    {
        public Team7 team { get; set; }
        public List<Leader2> leaders { get; set; }
    }

    public class HomeTeam
    {
        public string id { get; set; }
        public string gameProjection { get; set; }
        public string teamChanceLoss { get; set; }
    }

    public class AwayTeam
    {
        public string id { get; set; }
        public string gameProjection { get; set; }
        public string teamChanceLoss { get; set; }
    }

    public class Predictor
    {
        public string header { get; set; }
        public HomeTeam homeTeam { get; set; }
        public AwayTeam awayTeam { get; set; }
    }

    public class Provider
    {
        public string id { get; set; }
        public string name { get; set; }
        public int priority { get; set; }
    }

    public class SpreadRecord
    {
        public int wins { get; set; }
        public int losses { get; set; }
        public int pushes { get; set; }
        public string summary { get; set; }
    }

    public class AwayTeamOdds
    {
        public double winPercentage { get; set; }
        public bool favorite { get; set; }
        public bool underdog { get; set; }
        public int moneyLine { get; set; }
        public double spreadOdds { get; set; }
        public string teamId { get; set; }
        public double? averageScore { get; set; }
        public double? spreadReturn { get; set; }
        public SpreadRecord spreadRecord { get; set; }
    }

    public class SpreadRecord2
    {
        public int wins { get; set; }
        public int losses { get; set; }
        public int pushes { get; set; }
        public string summary { get; set; }
    }

    public class HomeTeamOdds
    {
        public double winPercentage { get; set; }
        public bool favorite { get; set; }
        public bool underdog { get; set; }
        public int moneyLine { get; set; }
        public double spreadOdds { get; set; }
        public string teamId { get; set; }
        public double? averageScore { get; set; }
        public double? spreadReturn { get; set; }
        public SpreadRecord2 spreadRecord { get; set; }
    }

    public class Pickcenter
    {
        public Provider provider { get; set; }
        public string details { get; set; }
        public double overUnder { get; set; }
        public double spread { get; set; }
        public AwayTeamOdds awayTeamOdds { get; set; }
        public HomeTeamOdds homeTeamOdds { get; set; }
        public List<object> links { get; set; }
    }

    public class Link3
    {
        public string href { get; set; }
        public string text { get; set; }
    }

    public class Team8
    {
        public string id { get; set; }
        public string uid { get; set; }
        public string displayName { get; set; }
        public string abbreviation { get; set; }
        public List<Link3> links { get; set; }
        public string logo { get; set; }
    }

    public class AgainstTheSpread
    {
        public Team8 team { get; set; }
        public List<object> records { get; set; }
    }

    public class Winprobability
    {
        public double tiePercentage { get; set; }
        public double homeWinPercentage { get; set; }
        public int secondsLeft { get; set; }
        public string playId { get; set; }
    }

    public class Link4
    {
        public string language { get; set; }
        public List<string> rel { get; set; }
        public string href { get; set; }
        public string text { get; set; }
        public string shortText { get; set; }
        public bool isExternal { get; set; }
        public bool isPremium { get; set; }
    }

    public class Image2
    {
        public string name { get; set; }
        public int width { get; set; }
        public int id { get; set; }
        public string credit { get; set; }
        public string type { get; set; }
        public string url { get; set; }
        public int height { get; set; }
        public string alt { get; set; }
        public string caption { get; set; }
    }

    public class News2
    {
        public string href { get; set; }
    }

    public class Self
    {
        public string href { get; set; }
    }

    public class Api
    {
        public News2 news { get; set; }
        public Self self { get; set; }
    }

    public class Short
    {
        public string href { get; set; }
    }

    public class Web
    {
        public string href { get; set; }
        public Short @short { get; set; }
    }

    public class Mobile
    {
        public string href { get; set; }
    }

    public class Links
    {
        public Api api { get; set; }
        public Web web { get; set; }
        public Mobile mobile { get; set; }
    }

    public class Leagues
    {
    }

    public class Api2
    {
        public Leagues leagues { get; set; }
    }

    public class Leagues2
    {
        public string href { get; set; }
    }

    public class Web2
    {
        public Leagues2 leagues { get; set; }
    }

    public class Leagues3
    {
        public string href { get; set; }
    }

    public class Mobile2
    {
        public Leagues3 leagues { get; set; }
    }

    public class Links2
    {
        public Api2 api { get; set; }
        public Web2 web { get; set; }
        public Mobile2 mobile { get; set; }
    }

    public class League
    {
        public int id { get; set; }
        public string description { get; set; }
        public Links2 links { get; set; }
    }

    public class Teams
    {
    }

    public class Api3
    {
        public Teams teams { get; set; }
    }

    public class Teams2
    {
        public string href { get; set; }
    }

    public class Web3
    {
        public Teams2 teams { get; set; }
    }

    public class Teams3
    {
        public string href { get; set; }
    }

    public class Mobile3
    {
        public Teams3 teams { get; set; }
    }

    public class Links3
    {
        public Api3 api { get; set; }
        public Web3 web { get; set; }
        public Mobile3 mobile { get; set; }
    }

    public class Team9
    {
        public int id { get; set; }
        public string description { get; set; }
        public Links3 links { get; set; }
    }

    public class Athletes
    {
    }

    public class Api4
    {
        public Athletes athletes { get; set; }
    }

    public class Athletes2
    {
        public string href { get; set; }
    }

    public class Web4
    {
        public Athletes2 athletes { get; set; }
    }

    public class Athletes3
    {
        public string href { get; set; }
    }

    public class Mobile4
    {
        public Athletes3 athletes { get; set; }
    }

    public class Links4
    {
        public Api4 api { get; set; }
        public Web4 web { get; set; }
        public Mobile4 mobile { get; set; }
    }

    public class Athlete2
    {
        public int id { get; set; }
        public string description { get; set; }
        public Links4 links { get; set; }
    }

    public class Category
    {
        public int id { get; set; }
        public string description { get; set; }
        public string type { get; set; }
        public int sportId { get; set; }
        public int leagueId { get; set; }
        public League league { get; set; }
        public string uid { get; set; }
        public DateTime createDate { get; set; }
        public int? teamId { get; set; }
        public Team9 team { get; set; }
        public int? athleteId { get; set; }
        public Athlete2 athlete { get; set; }
        public string guid { get; set; }
        public int? topicId { get; set; }
    }

    public class Article
    {
        public List<Image2> images { get; set; }
        public string description { get; set; }
        public Links links { get; set; }
        public List<Category> categories { get; set; }
        public string headline { get; set; }
    }

    public class News
    {
        public string header { get; set; }
        public Link4 link { get; set; }
        public List<Article> articles { get; set; }
    }

    public class Season
    {
        public int year { get; set; }
        public int type { get; set; }
    }

    public class Logo2
    {
        public string href { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public string alt { get; set; }
        public List<string> rel { get; set; }
    }

    public class Link5
    {
        public List<string> rel { get; set; }
        public string href { get; set; }
        public string text { get; set; }
    }

    public class Team10
    {
        public string id { get; set; }
        public string uid { get; set; }
        public string location { get; set; }
        public string name { get; set; }
        public string nickname { get; set; }
        public string abbreviation { get; set; }
        public string displayName { get; set; }
        public string color { get; set; }
        public string alternateColor { get; set; }
        public List<Logo2> logos { get; set; }
        public List<Link5> links { get; set; }
    }

    public class Linescore
    {
        public string displayValue { get; set; }
    }

    public class Record
    {
        public string type { get; set; }
        public string summary { get; set; }
        public string displayValue { get; set; }
    }

    public class Competitor
    {
        public string id { get; set; }
        public string uid { get; set; }
        public int order { get; set; }
        public string homeAway { get; set; }
        public bool winner { get; set; }
        public Team10 team { get; set; }
        public string score { get; set; }
        public List<Linescore> linescores { get; set; }
        public List<Record> record { get; set; }
        public bool possession { get; set; }
    }

    public class Type2
    {
        public string id { get; set; }
        public string name { get; set; }
        public string state { get; set; }
        public bool completed { get; set; }
        public string description { get; set; }
        public string detail { get; set; }
        public string shortDetail { get; set; }
    }

    public class Status
    {
        public Type2 type { get; set; }
    }

    public class Type3
    {
        public string id { get; set; }
        public string shortName { get; set; }
    }

    public class Market
    {
        public string id { get; set; }
        public string type { get; set; }
    }

    public class Media
    {
        public string shortName { get; set; }
    }

    public class Broadcast
    {
        public Type3 type { get; set; }
        public Market market { get; set; }
        public Media media { get; set; }
        public string lang { get; set; }
        public string region { get; set; }
    }

    public class Competition
    {
        public string id { get; set; }
        public string uid { get; set; }
        public string date { get; set; }
        public bool neutralSite { get; set; }
        public bool conferenceCompetition { get; set; }
        public bool boxscoreAvailable { get; set; }
        public bool commentaryAvailable { get; set; }
        public bool liveAvailable { get; set; }
        public bool onWatchESPN { get; set; }
        public bool recent { get; set; }
        public string boxscoreSource { get; set; }
        public string playByPlaySource { get; set; }
        public List<Competitor> competitors { get; set; }
        public Status status { get; set; }
        public List<Broadcast> broadcasts { get; set; }
    }

    public class Link6
    {
        public List<string> rel { get; set; }
        public string href { get; set; }
        public string text { get; set; }
        public string shortText { get; set; }
        public bool isExternal { get; set; }
        public bool isPremium { get; set; }
    }

    public class Link7
    {
        public List<string> rel { get; set; }
        public string href { get; set; }
        public string text { get; set; }
    }

    public class League2
    {
        public string id { get; set; }
        public string uid { get; set; }
        public string name { get; set; }
        public string abbreviation { get; set; }
        public string midsizeName { get; set; }
        public string slug { get; set; }
        public bool isTournament { get; set; }
        public List<Link7> links { get; set; }
    }

    public class Header
    {
        public string id { get; set; }
        public string uid { get; set; }
        public Season season { get; set; }
        public bool timeValid { get; set; }
        public List<Competition> competitions { get; set; }
        public List<Link6> links { get; set; }
        public int week { get; set; }
        public League2 league { get; set; }
    }

    public class Type4
    {
        public string id { get; set; }
    }

    public class Period4
    {
        public int number { get; set; }
    }

    public class Clock4
    {
        public double value { get; set; }
        public string displayValue { get; set; }
    }

    public class Link8
    {
        public string href { get; set; }
        public string text { get; set; }
    }

    public class Team11
    {
        public string id { get; set; }
        public string uid { get; set; }
        public string displayName { get; set; }
        public string abbreviation { get; set; }
        public List<Link8> links { get; set; }
        public string logo { get; set; }
    }

    public class ScoringType2
    {
        public string name { get; set; }
        public string displayName { get; set; }
        public string abbreviation { get; set; }
    }

    public class ScoringPlay
    {
        public string id { get; set; }
        public Type4 type { get; set; }
        public string text { get; set; }
        public int awayScore { get; set; }
        public int homeScore { get; set; }
        public Period4 period { get; set; }
        public Clock4 clock { get; set; }
        public Team11 team { get; set; }
        public ScoringType2 scoringType { get; set; }
    }

    public class News3
    {
        public string href { get; set; }
    }

    public class Events
    {
        public string href { get; set; }
    }

    public class Api5
    {
        public News3 news { get; set; }
        public Events events { get; set; }
    }

    public class Short2
    {
        public string href { get; set; }
    }

    public class Web5
    {
        public string href { get; set; }
        public Short2 @short { get; set; }
    }

    public class Sportscenter
    {
        public string href { get; set; }
    }

    public class App
    {
        public Sportscenter sportscenter { get; set; }
    }

    public class Mobile5
    {
        public string href { get; set; }
    }

    public class Links5
    {
        public Api5 api { get; set; }
        public Web5 web { get; set; }
        public App app { get; set; }
        public Mobile5 mobile { get; set; }
    }

    public class Leagues4
    {
        public string href { get; set; }
    }

    public class Api6
    {
        public Leagues4 leagues { get; set; }
    }

    public class Leagues5
    {
        public string href { get; set; }
    }

    public class Web6
    {
        public Leagues5 leagues { get; set; }
    }

    public class Leagues6
    {
        public string href { get; set; }
    }

    public class Mobile6
    {
        public Leagues6 leagues { get; set; }
    }

    public class Links6
    {
        public Api6 api { get; set; }
        public Web6 web { get; set; }
        public Mobile6 mobile { get; set; }
    }

    public class League3
    {
        public int id { get; set; }
        public string description { get; set; }
        public Links6 links { get; set; }
    }

    public class Teams4
    {
        public string href { get; set; }
    }

    public class Api7
    {
        public Teams4 teams { get; set; }
    }

    public class Teams5
    {
        public string href { get; set; }
    }

    public class Web7
    {
        public Teams5 teams { get; set; }
    }

    public class Teams6
    {
        public string href { get; set; }
    }

    public class Mobile7
    {
        public Teams6 teams { get; set; }
    }

    public class Links7
    {
        public Api7 api { get; set; }
        public Web7 web { get; set; }
        public Mobile7 mobile { get; set; }
    }

    public class Team12
    {
        public int id { get; set; }
        public string description { get; set; }
        public Links7 links { get; set; }
    }

    public class Category2
    {
        public int id { get; set; }
        public string description { get; set; }
        public string type { get; set; }
        public int sportId { get; set; }
        public int leagueId { get; set; }
        public League3 league { get; set; }
        public string uid { get; set; }
        public int? teamId { get; set; }
        public Team12 team { get; set; }
    }

    public class Metric
    {
        public int count { get; set; }
        public string type { get; set; }
    }

    public class Article2
    {
        public List<object> keywords { get; set; }
        public string description { get; set; }
        public string source { get; set; }
        public List<object> video { get; set; }
        public string type { get; set; }
        public string nowId { get; set; }
        public bool premium { get; set; }
        public List<object> related { get; set; }
        public bool allowSearch { get; set; }
        public Links5 links { get; set; }
        public int id { get; set; }
        public List<Category2> categories { get; set; }
        public string headline { get; set; }
        public string gameId { get; set; }
        public List<object> images { get; set; }
        public string linkText { get; set; }
        public DateTime published { get; set; }
        public bool allowComments { get; set; }
        public DateTime lastModified { get; set; }
        public List<Metric> metrics { get; set; }
        public List<object> inlines { get; set; }
        public string story { get; set; }
    }

    public class FullViewLink
    {
        public string text { get; set; }
        public string href { get; set; }
    }

    public class Stat
    {
        public string id { get; set; }
        public string name { get; set; }
        public string abbreviation { get; set; }
        public string displayName { get; set; }
        public string shortDisplayName { get; set; }
        public string description { get; set; }
        public string type { get; set; }
        public string summary { get; set; }
        public string displayValue { get; set; }
    }

    public class Logo3
    {
        public string href { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public string alt { get; set; }
        public List<string> rel { get; set; }
    }

    public class Entry
    {
        public string team { get; set; }
        public string link { get; set; }
        public string id { get; set; }
        public string uid { get; set; }
        public List<Stat> stats { get; set; }
        public List<Logo3> logo { get; set; }
    }

    public class Standings2
    {
        public List<Entry> entries { get; set; }
    }

    public class Stat2
    {
        public string id { get; set; }
        public string name { get; set; }
        public string abbreviation { get; set; }
        public string displayName { get; set; }
        public string shortDisplayName { get; set; }
        public string description { get; set; }
        public string type { get; set; }
        public string summary { get; set; }
        public string displayValue { get; set; }
    }

    public class Logo4
    {
        public string href { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public string alt { get; set; }
        public List<string> rel { get; set; }
    }

    public class Entry2
    {
        public string team { get; set; }
        public string link { get; set; }
        public string id { get; set; }
        public string uid { get; set; }
        public List<Stat2> stats { get; set; }
        public List<Logo4> logo { get; set; }
    }

    public class Standings3
    {
        public List<Entry2> entries { get; set; }
    }

    public class Division
    {
        public string header { get; set; }
        public Standings3 standings { get; set; }
        public string href { get; set; }
    }

    public class Group
    {
        public Standings2 standings { get; set; }
        public string header { get; set; }
        public string href { get; set; }
        public List<Division> divisions { get; set; }
    }

    public class Standings
    {
        public FullViewLink fullViewLink { get; set; }
        public List<Group> groups { get; set; }
    }

    public class GameSummary
    {
        public Boxscore boxscore { get; set; }
        public GameInfo gameInfo { get; set; }
        public Drives drives { get; set; }
        public List<Leader> leaders { get; set; }
        public List<object> broadcasts { get; set; }
        public Predictor predictor { get; set; }
        public List<Pickcenter> pickcenter { get; set; }
        public List<AgainstTheSpread> againstTheSpread { get; set; }
        public List<object> odds { get; set; }
        public List<Winprobability> winprobability { get; set; }
        public News news { get; set; }
        public Header header { get; set; }
        public List<ScoringPlay> scoringPlays { get; set; }
        public Article2 article { get; set; }
        public List<object> videos { get; set; }
        public Standings standings { get; set; }
    }
}