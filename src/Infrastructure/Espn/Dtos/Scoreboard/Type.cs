﻿namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class Type
    {
        public string id { get; set; }
        public int type { get; set; }
        public string name { get; set; }
        public string abbreviation { get; set; }
    }
}