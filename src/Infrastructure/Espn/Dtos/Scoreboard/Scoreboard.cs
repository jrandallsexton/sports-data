﻿using System.Collections.Generic;

namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class Scoreboard
    {
        public List<League> leagues { get; set; }
        public List<Event> events { get; set; }
    }
}
