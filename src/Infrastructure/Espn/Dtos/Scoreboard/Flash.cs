﻿namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class Flash
    {
        public string href { get; set; }
    }
}