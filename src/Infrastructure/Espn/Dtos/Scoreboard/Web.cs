﻿namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class Web
    {
        public string href { get; set; }
        public Short @short { get; set; }
        public Self2 self { get; set; }
    }
}