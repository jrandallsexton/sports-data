﻿using System.Collections.Generic;

namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class Link2
    {
        public List<string> rel { get; set; }
        public string href { get; set; }
    }
}