﻿namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class Address
    {
        public string city { get; set; }
        public string state { get; set; }
    }
}