﻿namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class Links
    {
        public Api api { get; set; }
        public Web web { get; set; }
        public Source source { get; set; }
        public Mobile mobile { get; set; }
    }
}