﻿namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class Status
    {
        public double clock { get; set; }
        public string displayClock { get; set; }
        public int period { get; set; }
        public Type3 type { get; set; }
    }
}