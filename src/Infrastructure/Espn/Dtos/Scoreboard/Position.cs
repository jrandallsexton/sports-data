﻿namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class Position
    {
        public string abbreviation { get; set; }
    }
}