﻿namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class HLS
    {
        public string href { get; set; }
        public HD HD { get; set; }
    }
}