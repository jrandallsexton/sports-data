﻿namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class Leader2
    {
        public string displayValue { get; set; }
        public double value { get; set; }
        public Athlete athlete { get; set; }
        public Team3 team { get; set; }
    }
}