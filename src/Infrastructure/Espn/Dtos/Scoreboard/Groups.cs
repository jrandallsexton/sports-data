﻿namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class Groups
    {
        public string id { get; set; }
        public string name { get; set; }
        public string shortName { get; set; }
        public bool isConference { get; set; }
    }
}