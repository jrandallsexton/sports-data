﻿using System.Collections.Generic;

namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class Competition
    {
        public string id { get; set; }
        public string uid { get; set; }
        public string date { get; set; }
        public int attendance { get; set; }
        public Type2 type { get; set; }
        public bool timeValid { get; set; }
        public bool neutralSite { get; set; }
        public bool conferenceCompetition { get; set; }
        public bool recent { get; set; }
        public Infrastructure.Espn.Dtos.Scoreboard.Venue venue { get; set; }
        public List<Competitor> competitors { get; set; }
        public List<object> notes { get; set; }
        public Status status { get; set; }
        public List<object> broadcasts { get; set; }
        public List<Leader> leaders { get; set; }
        public string startDate { get; set; }
        public List<object> geoBroadcasts { get; set; }
        public List<Headline> headlines { get; set; }
        public Groups groups { get; set; }
    }
}