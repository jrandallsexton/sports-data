﻿namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class Record
    {
        public string name { get; set; }
        public string abbreviation { get; set; }
        public string type { get; set; }
        public string summary { get; set; }
    }
}