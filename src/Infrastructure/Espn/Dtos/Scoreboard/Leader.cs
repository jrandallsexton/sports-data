﻿using System.Collections.Generic;

namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class Leader
    {
        public string name { get; set; }
        public string displayName { get; set; }
        public string shortDisplayName { get; set; }
        public string abbreviation { get; set; }
        public List<Leader2> leaders { get; set; }
    }
}