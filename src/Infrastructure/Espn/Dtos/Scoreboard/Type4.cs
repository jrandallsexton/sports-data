﻿namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class Type4
    {
        public string id { get; set; }
        public string name { get; set; }
        public string state { get; set; }
        public bool completed { get; set; }
        public string description { get; set; }
        public string detail { get; set; }
        public string shortDetail { get; set; }
    }
}