﻿namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class Tracking
    {
        public string sportName { get; set; }
        public string leagueName { get; set; }
        public string coverageType { get; set; }
        public string trackingName { get; set; }
        public string trackingId { get; set; }
    }
}