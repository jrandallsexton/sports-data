﻿using System.Collections.Generic;

namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class League
    {
        public string id { get; set; }
        public string uid { get; set; }
        public string name { get; set; }
        public string abbreviation { get; set; }
        public string midsizeName { get; set; }
        public string slug { get; set; }
        public Season season { get; set; }
        public string calendarType { get; set; }
        public bool calendarIsWhitelist { get; set; }
        public string calendarStartDate { get; set; }
        public string calendarEndDate { get; set; }
        public List<Calendar> calendar { get; set; }
    }
}