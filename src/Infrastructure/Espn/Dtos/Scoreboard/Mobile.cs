﻿namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class Mobile
    {
        public Alert alert { get; set; }
        public Source2 source { get; set; }
        public string href { get; set; }
        public Streaming streaming { get; set; }
        public ProgressiveDownload progressiveDownload { get; set; }
    }
}