﻿namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class Video
    {
        public int id { get; set; }
        public string source { get; set; }
        public string headline { get; set; }
        public string thumbnail { get; set; }
        public int duration { get; set; }
        public Tracking tracking { get; set; }
        public DeviceRestrictions deviceRestrictions { get; set; }
        public Links links { get; set; }
    }
}