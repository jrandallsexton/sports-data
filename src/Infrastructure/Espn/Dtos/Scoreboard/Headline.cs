﻿using System.Collections.Generic;

namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class Headline
    {
        public string description { get; set; }
        public string type { get; set; }
        public string shortLinkText { get; set; }
        public List<Video> video { get; set; }
    }
}