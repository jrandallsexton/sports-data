﻿namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class Self
    {
        public string href { get; set; }
    }
}