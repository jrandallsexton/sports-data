﻿namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class Api
    {
        public Self self { get; set; }
        public Artwork artwork { get; set; }
    }
}