﻿namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class Status2
    {
        public double clock { get; set; }
        public string displayClock { get; set; }
        public int period { get; set; }
        public Type4 type { get; set; }
    }
}