﻿namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class Season
    {
        public int year { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public Type type { get; set; }
    }
}