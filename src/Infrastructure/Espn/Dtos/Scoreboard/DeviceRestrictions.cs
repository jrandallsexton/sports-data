﻿using System.Collections.Generic;

namespace sportsData.Infrastructure.Espn.Dtos.Scoreboard
{
    public class DeviceRestrictions
    {
        public string type { get; set; }
        public List<string> devices { get; set; }
    }
}