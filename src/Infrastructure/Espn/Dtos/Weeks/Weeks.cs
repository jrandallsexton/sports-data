﻿
using System.Collections.Generic;

namespace sportsData.Infrastructure.Espn.Dtos.Weeks
{
    public class Item
    {
        public string href { get; set; }
    }

    public class Weeks
    {
        public int count { get; set; }

        public int pageIndex { get; set; }

        public int pageSize { get; set; }

        public int pageCount { get; set; }

        public List<Item> items { get; set; }
    }
}