﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

using sportsData.Common.Converters;

using System;
using System.Collections.Generic;
using System.Globalization;

namespace sportsData.Infrastructure.Espn.Dtos.Event
{
    public class Event
    {
        [JsonProperty("$ref")]
        public string Ref { get; set; }

        [JsonProperty("id")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Id { get; set; }

        [JsonProperty("uid")]
        public string Uid { get; set; }

        [JsonProperty("date")]
        public string Date { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("shortName")]
        public string ShortName { get; set; }

        [JsonProperty("season")]
        public TartuGecko Season { get; set; }

        [JsonProperty("seasonType")]
        public TartuGecko SeasonType { get; set; }

        [JsonProperty("week")]
        public TartuGecko Week { get; set; }

        [JsonProperty("timeValid")]
        public bool TimeValid { get; set; }

        [JsonProperty("competitions")]
        public List<Competition> Competitions { get; set; }

        [JsonProperty("links")]
        public List<Link> Links { get; set; }

        [JsonProperty("venues")]
        public List<TartuGecko> Venues { get; set; }

        [JsonProperty("league")]
        public TartuGecko League { get; set; }
    }

    public class Competition
    {
        [JsonProperty("$ref")]
        public string Ref { get; set; }

        [JsonProperty("id")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Id { get; set; }

        [JsonProperty("uid")]
        public string Uid { get; set; }

        [JsonProperty("date")]
        public string Date { get; set; }

        [JsonProperty("attendance")]
        public long Attendance { get; set; }

        [JsonProperty("type")]
        public TypeClass Type { get; set; }

        [JsonProperty("necessary")]
        public bool Necessary { get; set; }

        [JsonProperty("timeValid")]
        public bool TimeValid { get; set; }

        [JsonProperty("neutralSite")]
        public bool NeutralSite { get; set; }

        [JsonProperty("divisionCompetition")]
        public bool DivisionCompetition { get; set; }

        [JsonProperty("conferenceCompetition")]
        public bool ConferenceCompetition { get; set; }

        [JsonProperty("previewAvailable")]
        public bool PreviewAvailable { get; set; }

        [JsonProperty("recapAvailable")]
        public bool RecapAvailable { get; set; }

        [JsonProperty("boxscoreAvailable")]
        public bool BoxscoreAvailable { get; set; }

        [JsonProperty("lineupAvailable")]
        public bool LineupAvailable { get; set; }

        [JsonProperty("gamecastAvailable")]
        public bool GamecastAvailable { get; set; }

        [JsonProperty("playByPlayAvailable")]
        public bool PlayByPlayAvailable { get; set; }

        [JsonProperty("conversationAvailable")]
        public bool ConversationAvailable { get; set; }

        [JsonProperty("commentaryAvailable")]
        public bool CommentaryAvailable { get; set; }

        [JsonProperty("pickcenterAvailable")]
        public bool PickcenterAvailable { get; set; }

        [JsonProperty("summaryAvailable")]
        public bool SummaryAvailable { get; set; }

        [JsonProperty("liveAvailable")]
        public bool LiveAvailable { get; set; }

        [JsonProperty("ticketsAvailable")]
        public bool TicketsAvailable { get; set; }

        [JsonProperty("shotChartAvailable")]
        public bool ShotChartAvailable { get; set; }

        [JsonProperty("timeoutsAvailable")]
        public bool TimeoutsAvailable { get; set; }

        [JsonProperty("possessionArrowAvailable")]
        public bool PossessionArrowAvailable { get; set; }

        [JsonProperty("onWatchESPN")]
        public bool OnWatchEspn { get; set; }

        [JsonProperty("recent")]
        public bool Recent { get; set; }

        [JsonProperty("bracketAvailable")]
        public bool BracketAvailable { get; set; }

        [JsonProperty("gameSource")]
        public Source GameSource { get; set; }

        [JsonProperty("boxscoreSource")]
        public Source BoxscoreSource { get; set; }

        [JsonProperty("playByPlaySource")]
        public Source PlayByPlaySource { get; set; }

        [JsonProperty("linescoreSource")]
        public Source LinescoreSource { get; set; }

        [JsonProperty("statsSource")]
        public Source StatsSource { get; set; }

        [JsonProperty("venue")]
        public Venue Venue { get; set; }

        [JsonProperty("competitors")]
        public List<Competitor> Competitors { get; set; }

        [JsonProperty("notes")]
        public List<object> Notes { get; set; }

        [JsonProperty("situation")]
        public TartuGecko Situation { get; set; }

        [JsonProperty("status")]
        public TartuGecko Status { get; set; }

        [JsonProperty("odds")]
        public TartuGecko Odds { get; set; }

        [JsonProperty("broadcasts")]
        public TartuGecko Broadcasts { get; set; }

        [JsonProperty("details")]
        public TartuGecko Details { get; set; }

        [JsonProperty("leaders")]
        public TartuGecko Leaders { get; set; }

        [JsonProperty("links")]
        public List<Link> Links { get; set; }

        [JsonProperty("predictor")]
        public TartuGecko Predictor { get; set; }

        [JsonProperty("probabilities")]
        public TartuGecko Probabilities { get; set; }

        [JsonProperty("powerIndexes")]
        public TartuGecko PowerIndexes { get; set; }

        [JsonProperty("drives")]
        public TartuGecko Drives { get; set; }
    }

    public class Source
    {
        [JsonProperty("id")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Id { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }
    }

    public class TartuGecko
    {
        [JsonProperty("$ref")]
        public string Ref { get; set; }
    }

    public class Competitor
    {
        [JsonProperty("$ref")]
        public string Ref { get; set; }

        [JsonProperty("id")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Id { get; set; }

        [JsonProperty("uid")]
        public string Uid { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("order")]
        public long Order { get; set; }

        [JsonProperty("homeAway")]
        public string HomeAway { get; set; }

        [JsonProperty("winner")]
        public bool Winner { get; set; }

        [JsonProperty("team")]
        public TartuGecko Team { get; set; }

        [JsonProperty("score")]
        public TartuGecko Score { get; set; }

        [JsonProperty("linescores")]
        public TartuGecko Linescores { get; set; }

        [JsonProperty("roster")]
        public TartuGecko Roster { get; set; }

        [JsonProperty("statistics")]
        public TartuGecko Statistics { get; set; }

        [JsonProperty("leaders")]
        public TartuGecko Leaders { get; set; }

        [JsonProperty("record")]
        public TartuGecko Record { get; set; }

        [JsonProperty("curatedRank")]
        public CuratedRank CuratedRank { get; set; }

        [JsonProperty("previousCompetition")]
        public TartuGecko PreviousCompetition { get; set; }

        [JsonProperty("nextCompetition")]
        public TartuGecko NextCompetition { get; set; }
    }

    public class CuratedRank
    {
        [JsonProperty("current")]
        public long Current { get; set; }
    }

    public class Link
    {
        [JsonProperty("language")]
        public Language Language { get; set; }

        [JsonProperty("rel")]
        public List<string> Rel { get; set; }

        [JsonProperty("href")]
        public string Href { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("shortText")]
        public string ShortText { get; set; }

        [JsonProperty("isExternal")]
        public bool IsExternal { get; set; }

        [JsonProperty("isPremium")]
        public bool IsPremium { get; set; }
    }

    public class TypeClass
    {
        [JsonProperty("id")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Id { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("abbreviation")]
        public string Abbreviation { get; set; }
    }

    public class Venue
    {
        [JsonProperty("$ref")]
        public string Ref { get; set; }

        [JsonProperty("id")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Id { get; set; }

        [JsonProperty("fullName")]
        public string FullName { get; set; }

        [JsonProperty("shortName")]
        public string ShortName { get; set; }

        [JsonProperty("address")]
        public Address Address { get; set; }

        [JsonProperty("capacity")]
        public long Capacity { get; set; }

        [JsonProperty("grass")]
        public bool Grass { get; set; }

        [JsonProperty("indoor")]
        public bool Indoor { get; set; }

        [JsonProperty("images")]
        public List<Image> Images { get; set; }
    }

    public class Address
    {
        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("zipCode")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long ZipCode { get; set; }
    }

    public class Image
    {
        [JsonProperty("href")]
        public Uri Href { get; set; }

        [JsonProperty("width")]
        public long Width { get; set; }

        [JsonProperty("height")]
        public long Height { get; set; }

        [JsonProperty("alt")]
        public string Alt { get; set; }

        [JsonProperty("rel")]
        public List<string> Rel { get; set; }
    }

    public enum Language { En };

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                LanguageConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class LanguageConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Language) || t == typeof(Language?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "en")
            {
                return Language.En;
            }
            throw new Exception("Cannot unmarshal type Language");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (Language)untypedValue;
            if (value == Language.En)
            {
                serializer.Serialize(writer, "en");
                return;
            }
            throw new Exception("Cannot marshal type Language");
        }

        public static readonly LanguageConverter Singleton = new LanguageConverter();
    }
}