﻿using Newtonsoft.Json;

using sportsData.Common.Converters;

using System;
using System.Collections.Generic;

namespace sportsData.Infrastructure.Espn.Dtos.Season
{
    public class Season
    {
        [JsonProperty("$ref", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Ref { get; set; }

        [JsonProperty("year", NullValueHandling = NullValueHandling.Ignore)]
        public long? Year { get; set; }

        [JsonProperty("startDate", NullValueHandling = NullValueHandling.Ignore)]
        public string StartDate { get; set; }

        [JsonProperty("endDate", NullValueHandling = NullValueHandling.Ignore)]
        public string EndDate { get; set; }

        [JsonProperty("displayName", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? DisplayName { get; set; }

        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public TypeElement Type { get; set; }

        [JsonProperty("types", NullValueHandling = NullValueHandling.Ignore)]
        public Types Types { get; set; }

        [JsonProperty("rankings", NullValueHandling = NullValueHandling.Ignore)]
        public Awards Rankings { get; set; }

        [JsonProperty("awards", NullValueHandling = NullValueHandling.Ignore)]
        public Awards Awards { get; set; }

        [JsonProperty("futures", NullValueHandling = NullValueHandling.Ignore)]
        public Awards Futures { get; set; }

        [JsonProperty("leaders", NullValueHandling = NullValueHandling.Ignore)]
        public Awards Leaders { get; set; }
    }

    public class Awards
    {
        [JsonProperty("$ref", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Ref { get; set; }
    }

    public class TypeElement
    {
        [JsonProperty("$ref", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Ref { get; set; }

        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? Id { get; set; }

        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public long? Type { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("abbreviation", NullValueHandling = NullValueHandling.Ignore)]
        public string Abbreviation { get; set; }

        [JsonProperty("year", NullValueHandling = NullValueHandling.Ignore)]
        public long? Year { get; set; }

        [JsonProperty("startDate", NullValueHandling = NullValueHandling.Ignore)]
        public string StartDate { get; set; }

        [JsonProperty("endDate", NullValueHandling = NullValueHandling.Ignore)]
        public string EndDate { get; set; }

        [JsonProperty("hasGroups", NullValueHandling = NullValueHandling.Ignore)]
        public bool? HasGroups { get; set; }

        [JsonProperty("hasStandings", NullValueHandling = NullValueHandling.Ignore)]
        public bool? HasStandings { get; set; }

        [JsonProperty("hasLegs", NullValueHandling = NullValueHandling.Ignore)]
        public bool? HasLegs { get; set; }

        [JsonProperty("groups", NullValueHandling = NullValueHandling.Ignore)]
        public Awards Groups { get; set; }

        [JsonProperty("weeks", NullValueHandling = NullValueHandling.Ignore)]
        public Awards Weeks { get; set; }

        [JsonProperty("leaders", NullValueHandling = NullValueHandling.Ignore)]
        public Awards Leaders { get; set; }
    }

    public class Types
    {
        [JsonProperty("$ref", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Ref { get; set; }

        [JsonProperty("count", NullValueHandling = NullValueHandling.Ignore)]
        public long? Count { get; set; }

        [JsonProperty("pageIndex", NullValueHandling = NullValueHandling.Ignore)]
        public long? PageIndex { get; set; }

        [JsonProperty("pageSize", NullValueHandling = NullValueHandling.Ignore)]
        public long? PageSize { get; set; }

        [JsonProperty("pageCount", NullValueHandling = NullValueHandling.Ignore)]
        public long? PageCount { get; set; }

        [JsonProperty("items", NullValueHandling = NullValueHandling.Ignore)]
        public List<TypeElement> Items { get; set; }
    }
}