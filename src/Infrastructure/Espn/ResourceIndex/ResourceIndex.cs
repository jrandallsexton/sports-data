﻿
using Newtonsoft.Json;

using System.Collections.Generic;

namespace sportsData.Infrastructure.Espn.ResourceIndex
{
    public class Item
    {
        [JsonProperty("$ref")]
        public string href { get; set; }
        public int id { get; set; }
    }

    public class ResourceIndex
    {
        public int count { get; set; }
        public int pageIndex { get; set; }
        public int pageSize { get; set; }
        public int pageCount { get; set; }
        public List<Item> items { get; set; }
    }
}