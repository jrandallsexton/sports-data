﻿using sportsData.Infrastructure.Espn.Dtos.Award;
using sportsData.Infrastructure.Espn.Dtos.Franchise;
using sportsData.Infrastructure.Espn.Dtos.TeamInformation;

using System.Collections.Generic;
using System.Threading.Tasks;

using Venue = sportsData.Infrastructure.Espn.Dtos.Venue.Venue;

namespace sportsData.Infrastructure.Espn
{
    public interface IProvideEspnApiData
    {
        Task<ResourceIndex.ResourceIndex> Awards(int franchiseId);
        Task<List<Award>> AwardsByFranchise(int franchiseId);
        Task<Franchise> Franchise(int franchiseId);
        Task<ResourceIndex.ResourceIndex> Franchises();
        Task<Infrastructure.Espn.Dtos.Team.Team> EspnTeam(int fourDigitYear, int teamId);
        Task<TeamInformation> TeamInformation(int teamId);
        Task<ResourceIndex.ResourceIndex> Teams(int fourDigitYear);
        Task<ResourceIndex.ResourceIndex> Venues();
        Task<Venue> Venue(int venueId, bool ignoreCache);
        Task<byte[]> GetMedia(string uri);
    }
}