﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sportsData.Infrastructure.Espn
{
    public class EspnApiClientConfig
    {
        public string DataDirectory { get; set; }
        public string MediaDirectory { get; set; }
    }
}