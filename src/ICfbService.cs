﻿
using sportsData.Infrastructure.Espn.Dtos.Award;
using sportsData.Infrastructure.Espn.Dtos.GameSummary;
using sportsData.Infrastructure.Espn.Dtos.Scoreboard;
using sportsData.Infrastructure.Espn.Dtos.TeamInformation;
using sportsData.Infrastructure.Espn.Dtos.Weeks;
using sportsData.Infrastructure.Espn.ResourceIndex;
using sportsData.Infrastructure.SportsDataIo.Dtos;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Season = sportsData.Infrastructure.Espn.Dtos.GameSummary.Season;
using Team = sportsData.Infrastructure.SportsDataIo.Dtos.Team;
using Teams = sportsData.Infrastructure.Espn.Dtos.Teams.Teams;

namespace sportsData
{
    public interface ICfbService
    {
        bool AreAnyGamesInProgress();

        List<Conference> LeagueHierarchy();

        int CurrentSeason();

        CurrentSeasonDetails CurrentSeasonDetails();

        string CurrentSeasonType();

        int CurrentWeek();

        // TODO: Player Details by Active
        // https://api.sportsdata.io/v3/cfb/stats/json/Players

        // TODO: Player Details by Player
        // https://api.sportsdata.io/v3/cfb/stats/json/Player/{playerid}

        // TODO: Player Details by Team
        // https://api.sportsdata.io/v3/cfb/stats/json/Players/{team}

        List<Stadium> Stadiums();

        List<Team> Teams();

        // TODO: Games by Date
        // https://api.sportsdata.io/v3/cfb/scores/json/GamesByDate/{date}

        List<Game> GamesByWeek(int season, int week);

        List<ScheduledGame> Schedules(int season);

        Scoreboard Scoreboard(int yyyymmdd, bool useCache = true);

        Task<Scoreboard> ScoreboardAsync(int yyyymmdd);

        GameSummary GameSummary(int espnGameId);

        TeamInformation TeamInformation(string espnTeamAbbreviation);

        //TeamInformation TeamInformation(int espnTeamId);

        Weeks Weeks(int season, int seasonType);

        Event Event(int eventId);

        Season Season(int year);

        Infrastructure.Espn.Dtos.Team.Team EspnTeam(int year, int teamId);

        string PerformHttpGet(Uri uri, bool isEspn = false, bool useCache = true);

        //ResourceIndex Awards(int franchiseId);

        Award Award(string url);
    }
}